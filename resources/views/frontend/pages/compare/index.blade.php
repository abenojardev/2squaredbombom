@extends('frontend.components.layouts.main.index')
@section('title',$title)
@section('content')  
	<section class="compare-padding">
	    <div class="container">
	        <div class="row">
	            <div class="col-sm-12">
	                <div class="compare-page">
	                    <div class="table-wrapper table-responsive">
                        	@if(count($products) != 0)
	                        	<a href="{{ URL::route('client.compare.clear') }}" class="pull-right">Clear items x</a><br><br><br>
		                        <table class="table"> 
		                            <tbody id="table-compare">
			                            <tr>
			                                <th class="product-name">Product Name</th>
			                                @foreach($products as $x)
			                                	<td class="grid-link__title">{{ $x->name }}</td> 
			                                @endforeach
			                            </tr>
			                            <tr>
			                                <th class="product-name">Product Price</th>
			                                @foreach($products as $x)
			                                	<td class="grid-link__title">₱ {{ number_format($x->srp,2,'.',',') }}</td> 
			                                @endforeach
			                            </tr>
			                            <tr>
			                                <th class="product-name">Product Image</th> 
			                                @foreach($products as $x)
				                                <td class="item-row">
				                                	<img src="{{ URL::route('app.resources').'?src='.$x->photo }}" alt="" class="featured-image"> 
				                                </td> 
			                                @endforeach
			                                
			                            </tr>
			                            <tr>
			                                <th class="product-name">Product Part Number</th>
			                                @foreach($products as $x)
			                                	<td class="grid-link__title">{{ $x->part_number }}</td> 
			                                @endforeach
			                            </tr>
			                            <tr>
			                                <th class="product-name">Product Category</th>
			                                @foreach($products as $x)
			                                	<td class="grid-link__title">{{ App\Models\ProductCategories::find($x->category_id)->title }}</td> 
			                                @endforeach
			                            </tr>
			                            <tr>
			                                <th class="product-name">Product Description</th> 
			                                @foreach($products as $x)
			                                	<td class="item-row">
				                                    <p class="description-compare">{{ $x->description }}</p>
				                                </td>
			                                @endforeach 
			                            </tr>
			                            <tr>
			                                <th class="product-name">Availability</th>
			                                @foreach($products as $x)
			                                	<td class="available-stock">
			                                		@if($x->is_available == 1)
			                                			Available
			                                		@else
			                                			Not Available
			                                		@endif
				                                </td>
			                                @endforeach 
			                            </tr>
			                            <tr> 
			                                <th class="product-name">Filters</th>
			                                @foreach($products as $x)
			                                	<td class="available-stock">
			                                		@foreach($x->filters as $k => $y)
			                                			<b>{{ App\Models\ProductFilters::find($k)->title }}</b> :   
			                                			<span class="pull-right">{{ App\Models\ProductFiltersItems::find($y)->type }}</span>
			                                			<br>
			                                		@endforeach
				                                </td>
			                                @endforeach 
			                            </tr>
			                            <tr>
			                                <th class="product-name">Action</th>
			                                @foreach($products as $x)
				                                <td class="available-stock"> 
				                                	<center>
			                                        	<a href="{{ URL::route('client.products',str_replace(' ','-',strtolower($x->name))) }}" class="add-to-cart btn btn-solid" style="color: white;">View Product</a> 
			                                    	</center>
				                                </td>
			                                @endforeach
			                            </tr>
		                            </tbody>
		                        </table>
		                    @else
		                    	<center>
		                    		<p>
		                    			No items added yet !
		                    		</p>
		                    	</center>
		                    @endif
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</section>
 
	@include('frontend.components.ads.landscape.index')
	<br><br>
@endsection