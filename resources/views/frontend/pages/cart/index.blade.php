@extends('frontend.components.layouts.main.index')
@section('title',$title)
@section('content')  
	<section class="cart-section section-b-space">
	    <div class="container">
        	@if($items && count($items) != 0)
		        <div class="row">
		            <div class="col-sm-12">
		                <table class="table cart-table table-responsive-xs">
		                    <thead>
			                    <tr class="table-head"> 
			                        <th scope="col">Product</th>
			                        <th scope="col">Variations</th>
			                        <th scope="col">Part Number</th>
			                        <th scope="col">SRP</th>
			                        <th scope="col">Quantity</th>  
			                        <th scope="col">Total</th>   
			                    </tr>
		                    </thead> 
		                    <tbody>
		                    	@php $total = 0; @endphp
		                    	@foreach($items as $k => $x)
		                    		@php $total += ($x['qty'] * App\Models\Product::find($x['product_id'])->srp); @endphp
		                    		@php $partnumber = App\Models\Product::find($x['product_id'])->part_number; @endphp
				                    <tr>  
				                        <td>
				                        	{{ ($k+1) }}. {{ App\Models\Product::find($x['product_id'])->name }} 
				                        </td> 
				                        <td></td>
				                        <td>{{ $partnumber }}</td>
				                        <td>
				                            <b class="td-color"> 				                            	
				                        		₱ {{ number_format(App\Models\Product::find($x['product_id'])->srp,2,'.',',') }} 
				                            </b>
				                        </td>
				                        <td>{{ $x['qty'] }}</td>
				                        <td>₱ {{ number_format($x['qty'] * App\Models\Product::find($x['product_id'])->srp,2,'.',',') }}</td> 
				                    </tr>
				                    @foreach($x['variations'] as $z =>  $y)
				                    	@php $total += ($x['qty'] * $y['items']['srp']); @endphp
		                    			@php $partnumber = $partnumber.'_'.$y['items']['partnumber'] @endphp
					                    <tr>  
					                        <td></td> 
					                        <td>{{ $y['type'] }} : {{ $y['items']['name'] }}</td>
					                        <td>{{ $y['items']['partnumber'] }}</td>
					                        <td>
					                            <b class="td-color"> 				                            	
					                        		₱ {{ number_format($y['items']['srp'],2,'.',',') }} 
					                            </b>
					                        </td>
					                        <td>{{ $x['qty'] }}</td>
					                        <td>₱ {{ number_format($x['qty'] * $y['items']['srp'],2,'.',',') }}</td> 
					                    </tr>
				                    @endforeach
				                    <td>
				                    	<td>Final Part Number</td>
				                    	<td><b>{{ $partnumber }}</b></td>
				                    	<td></td>
				                    	<td></td>
				                    	<td></td> 
				                    </td>
			                    @endforeach 
		                </table>
		                <table class="table cart-table table-responsive-md">
		                    <tfoot>
		                    <tr> 
		                        <td>
		                            <h2>₱ {{ number_format($total,2,'.',',') }}</h2></td>
		                    </tr>
		                    </tfoot>
		                </table>
		            </div>
		        </div>

		        <div class="row cart-buttons">
		            <div class="col-6"><a href="{{ URL::route('client.cart.clear') }}" class="btn btn-solid">clear Cart</a></div>
		            <div class="col-6"><a href="{{ URL::route('client.cart.checkout') }}" class="btn btn-solid">check out</a></div>
		        </div>
            @else
            	<center>No items yet !</center>
            @endif
	    </div>
	</section> 
	@include('frontend.components.ads.landscape.index')
	<br><br> 
@endsection