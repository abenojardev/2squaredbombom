@extends('frontend.components.layouts.main.index')
@section('title',$title)
@section('content')  
	<section class="section-b-space">
	    <div class="container">
	        <div class="checkout-page">
	            <div class="checkout-form">
	                <form action="{{ URL::route('client.checkout.save') }}" method="post">
	                	@csrf
	                    <div class="row">
	                        <div class="col-lg-12 col-sm-12 col-xs-12">
	                            <div class="checkout-title">
	                                <h3>Checkout Details</h3></div>
	                            <div class="row check-out">
	                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
	                                    <div class="field-label">Company Name</div>
	                                    <input type="text" name="company_name" required>
	                                </div>
	                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
	                                    <div class="field-label">Country</div>
	                                    <input type="text" name="country" required>
	                                </div>
	                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
	                                    <div class="field-label">Date Needed</div>
	                                    <input type="text" name="date_needed" required>
	                                </div>
	                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
	                                    <div class="field-label">Estimated Order Schededule</div>
	                                    <input type="text" name="estimatedordersched" required>
	                                </div>  
	                            </div>
	                        </div> 

                            <div class="col-md-12">
                                <button class="btn btn-solid" type="submit">Continue</button>
                            </div>
	                    </div>
	                </form>
	            </div>
	        </div>
	    </div>
	</section>
	@include('frontend.components.ads.landscape.index')
	<br><br> 
@endsection