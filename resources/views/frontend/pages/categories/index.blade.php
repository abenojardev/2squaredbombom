@extends('frontend.components.layouts.main.index')
@section('title',$title)
@section('content')  
	
	@if(count($categories['child']) <= 0) 
		@include('frontend.pages.products.cards.index',[ 'products' => $products ])
	@else
		@include('frontend.pages.categories.cards.index',[ 'categories' => $categories ])
	@endif
 
	@include('frontend.components.ads.landscape.index')
	<br><br>
@endsection