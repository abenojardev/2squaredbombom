<section class="ratio_45">
    <div class="container">
         <div class="row">
            <div class="col">
                <div class="title4">
                    <h2 class="title-inner4">{{ $categories['parent']->title }}</h2>
                        <div class="line"><span></span></div> 
                </div>
            </div>
        </div>
        <div class="row partition3"> 
        	@foreach($categories['child'] as $x)
	            <div class="col-md-4">
	                <a href="{{ URL::route('client.categories',str_replace(' ','-',strtolower($x->title))) }}">
	                    <div class="collection-banner p-left">
	                        <div class="img-part">
	                            <img src="{{ URL::route('app.resources').'?src='.$x->photo }}" class="img-fluid blur-up lazyload bg-img" alt="">
	                        </div>
	                        <div class="contain-banner banner-3">
	                            <div>
	                                <h4>{{ $x->title }}</h4> 
	                            </div>
	                        </div>
	                    </div>
	                </a>
	            </div>  
            @endforeach
        </div>
    </div>
</section>