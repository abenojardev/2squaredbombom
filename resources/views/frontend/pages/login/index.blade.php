@extends('frontend.components.layouts.main.index')
@section('title',$title)
@section('content')  
	<section class="login-page section-b-space">
	    <div class="container">
	        <div class="row">
	            <div class="col-lg-6">
	                <h3>Login</h3>
	                <div class="theme-card">
	                    <form class="theme-form" method="post" action="{{ URL::route('client.verify') }}">
	                    	@csrf
	                    	@if(session('error'))
	                    		<span style="color:red !important;">{{ session('error') }}</span>
	                    	@endif 
	                        <div class="form-group">
	                            <label for="email">Email</label>
	                            <input type="text" name="email" class="form-control" id="email" placeholder="Email" required="">
	                        </div>
	                        <div class="form-group">
	                            <label for="review">Password</label>
	                            <input type="password" name="password"  class="form-control" id="review" placeholder="Enter your password" required="">
	                        </div>
	                        <button class="btn btn-solid">Login</button>
	                    </form>
	                </div>
	            </div>
	            <div class="col-lg-6 right-login">
	                <h3>New Customer</h3>
	                <div class="theme-card authentication-right">
	                    <h6 class="title-font">Create A Account</h6>

	                    <form class="theme-form" method="post" action="{{ URL::route('client.register') }}">
	                    	@csrf
	                    	@if(session('success'))
	                    		<span style="color:green !important;">{{ session('success') }}</span>
	                    	@endif 
	                        <div class="form-group">
	                            <label for="email">Fullname</label>
	                            <input type="text" name="name" class="form-control" id="email" placeholder="Fullname" required="">
	                        </div>
	                        <div class="form-group">
	                            <label for="email">Email</label>
	                            <input type="text" name="email" class="form-control" id="email" placeholder="Email" required="">
	                        </div>
	                        <div class="form-group">
	                            <label for="review">Password</label>
	                            <input type="password" name="password"  class="form-control" id="review" placeholder="Enter your password" required="">
	                        </div>
	                        <div class="form-group">
	                            <label for="review">Re-type Password</label>
	                            <input type="password" name="repassword"  class="form-control" id="review" placeholder="Retype your password" required="">
	                        </div>
	                        <button class="btn btn-solid">Register</button>
	                    </form>
	            </div>
	        </div>
	    </div>
	</section>
	@include('frontend.components.ads.landscape.index')
	<br><br> 
@endsection