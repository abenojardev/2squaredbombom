@extends('frontend.components.layouts.main.index')
@section('title',$title)
@section('content')  
    <section>
        <div class="collection-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-lg-5 col-sm-10 col-xs-12">
                        <div class="product-right-slick">
                            <div>
                                <img src="{{ URL::route('app.resources').'?src='.$data->photo }}" width="100%" class="img-fluid blur-up lazyload image_zoom_cls-0"> 
                            </div> 
                            <div>
                                <img src="{{ URL::route('app.resources').'?src='.$data->photo }}" width="100%" class="img-fluid blur-up lazyload image_zoom_cls-0"> 
                            </div> 
                            <div>
                                <img src="{{ URL::route('app.resources').'?src='.$data->photo }}" width="100%" class="img-fluid blur-up lazyload image_zoom_cls-0"> 
                            </div> 
                        </div>
                    </div>
                    <div class="col-lg-1 col-sm-2 col-xs-12">
                        <div class="row">
                            <div class="col-12 p-0">
                                <div class="slider-right-nav">
                                    <div><img src="{{ URL::route('app.resources').'?src='.$data->photo }}" alt="" class="img-fluid blur-up lazyload"></div>
                                    <div><img src="{{ URL::route('app.resources').'?src='.$data->photo }}" alt="" class="img-fluid blur-up lazyload"></div>
                                    <div><img src="{{ URL::route('app.resources').'?src='.$data->photo }}" alt="" class="img-fluid blur-up lazyload"></div> 
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 rtl-text">
                        <div class="product-right">
                            <h2>{{ $data->name }}</h2> 
                            <h3>₱ {{ number_format($data->srp,2,'.',',') }}</h3>  
                            <div class="border-product">
                                <h6 class="product-title">Category</h6>
                                <p>{{ App\Models\ProductCategories::find($data->category_id)->title }}</p><br>
                                <h6 class="product-title">Part Number</h6>
                                <p>{{ App\Models\ProductCategories::find($data->category_id)->title }}</p><br>
                                <h6 class="product-title">Stocks</h6>
                                <p>{{ is_null($data->stocks) ? 0 : $data->stocks }} pcs</p><br>
                                <h6 class="product-title">Availability</h6>
                                <p>{{ ($data->is_available == 0) ? 'Not Available' : 'Available' }}</p><br>
                                <h6 class="product-title">product details</h6>
                                <p>{{ $data->description }}</p>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="tab-product m-0">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-lg-12">
                    <ul class="nav nav-tabs nav-material" id="top-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="review-top-tab" data-toggle="tab" href="#top-cart" role="tab" aria-selected="false">Add to cart</a>
                            <div class="material-border"></div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="top-home-tab" data-toggle="tab" href="#top-desc" role="tab" aria-selected="true">Description</a>
                            <div class="material-border"></div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="profile-top-tab" data-toggle="tab" href="#top-app" role="tab" aria-selected="false">Applications</a>
                            <div class="material-border"></div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="contact-top-tab" data-toggle="tab" href="#top-docs" role="tab" aria-selected="false">Documets</a>
                            <div class="material-border"></div>
                        </li>
                    </ul>
                    <div class="tab-content nav-material" id="top-tabContent"> 
                        <div class="tab-pane fade" id="top-desc" role="tabpanel" aria-labelledby="desc-top-tab"> 
                            <p>{{ $data->description }}</p>
                        </div> 
                        <div class="tab-pane fade" id="top-app" role="tabpanel" aria-labelledby="app-top-tab"> 
                            <p>No applications</p>
                        </div> 
                        <div class="tab-pane fade" id="top-docs" role="tabpanel" aria-labelledby="docs-top-tab"> 
                            <p>No documents</p>
                        </div> 
                        <div class="tab-pane fade active show" id="top-cart" role="tabpanel" aria-labelledby="cart-top-tab"> 
                            <form class="theme-form" method="post" action="{{ URL::route('client.products.addtocart') }}">
                                @csrf
                                <div class="form-row"> 
                                    <div class="col-md-12">
                                        <br>
                                    </div>
                                    <div class="col-md-3">
                                        <label for="name">Quantity</label>
                                        <input type="number" name="qty" class="form-control" value="1" min="1" required>
                                        <input type="hidden" name="product_id" value="{{ $data->id }}">
                                        <input type="hidden" name="parent_id" value="{{ $data->user_id }}">
                                    </div> 
                                    @if(!is_null($data->filters))
                                        @foreach($data->specs as $k => $x)
                                            <div class="col-md-12">
                                                <h5>Choose {{ $x['name'] }}</h5>
                                                <div class="bundle"> 
                                                    <div class="bundle_detail">
                                                        <div class="theme_checkbox">
                                                            @foreach($x['items'] as $y)
                                                                <label>
                                                                    {{ $y['name'] }}
                                                                    <span class="price_product">
                                                                        <small>(+₱ {{ $y['srp'] }})</small> 
                                                                    </span>
                                                                    <input type="radio" required name="variations[{{ $k }}]" value="{{ json_encode([ 'type' => $x['name'],'items' => $y]) }}">
                                                                    <span class="checkmark"></span>
                                                                </label>  
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr>
                                            </div>
                                        @endforeach
                                    @endif
                                    <div class="col-md-12">
                                        <button class="btn btn-solid" type="submit">Add to Cart</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include('frontend.components.ads.landscape.index')
    <br><br>
@endsection