<div class="collection-filter-block"> 
    <div class="collection-mobile-back"><span class="filter-back"><i class="fa fa-angle-left" aria-hidden="true"></i> back</span></div>

    @if(count($filters) <= 0)
        <br>
        <center>No filters</center>
        <br>
    @else
        <br>
        <a href="{{ URL::current() }}">Clear filters</a>
        <form method="get">
            @foreach($filters as $k => $x)
                <div class="collection-collapse-block @if($k <= 0 || isset($_GET[$x->id])) open @endif">
                    <h3 class="collapse-block-title">{{ $x->title }}</h3>
                    <div class="collection-collapse-block-content" @if($k != 0 && !isset($_GET[$x->id])) style="display: none;" @endif>
                        <div class="collection-brand-filter">
                            @foreach($x->items as $y)
                                <div class="custom-control custom-checkbox collection-filter-checkbox">
                                    <input type="radio" name="{{ $x->id }}" value="{{ $y->id }}" @if(isset($_GET[$x->id]) && $_GET[$x->id] == $y->id) checked @endif>
                                    <label for="{{ $x->id }}">{{ $y->type }}</label>
                                </div> 
                            @endforeach
                        </div>
                    </div>
                </div> 
            @endforeach

            @if($filters)
                <br>
                <center>
                    <button class="btn btn-warning btn-lg">Apply Filters</button>
                </center>
                <br>
            @endif 
        </form>
    @endif
</div>