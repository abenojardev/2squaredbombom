<section class="section-b-space ratio_asos">
    <div class="collection-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-sm-3 collection-filter">  
                    @include('frontend.pages.products.filters.index',[ 'filters' => $products['filters'] ])
                    @include('frontend.components.ads.portrait.index')
                </div>
                <div class="collection-content col">
                    <div class="page-main-content">
                        <div class="row">
                            <div class="col-sm-12"> 
                                <div class="collection-product-wrapper">
                                    <div class="row">
                                        <div class="col-lg-6 xoffset-lg-3"> 
                                            <div class="input-group">
                                                <p>
                                                    <br><br>
                                                    Showing {{ count($products['products']) }} of {{ count($products['products']) }}
                                                </p>
                                            </div> 
                                        </div>
                                        <div class="col-lg-6 xoffset-lg-3">
                                            <form class="form-header">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" name="q" placeholder="Search products on this category">
                                                    @foreach($_GET as $k => $x)
                                                        @if($k != 'q')
                                                            <input type="hidden" name="{{ $k }}" value="{{ $x }}">
                                                        @endif
                                                    @endforeach
                                                    <div class="input-group-append">
                                                        <button class="btn btn-solid"><i class="fa fa-search"></i>Search</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="product-top-filter">
                                        <div class="row">
                                            <div class="col-xl-12">
                                                <div class="filter-main-btn"><span class="filter-btn btn btn-theme"><i class="fa fa-filter" aria-hidden="true"></i> Filter</span></div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="product-filter-content"> 
                                                    <div class="collection-view">
                                                        <ul>
                                                            <li><i class="fa fa-th grid-layout-view"></i></li>
                                                            <li><i class="fa fa-list-ul list-layout-view"></i></li>
                                                        </ul>
                                                    </div> 
                                                    <div class="product-page-filter">
                                                        <select>
                                                            <option value="High to low">Sorting items</option>
                                                            <option value="Low to High">Price High to low</option>
                                                            <option value="Low to High">Price Low to High</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="product-wrapper-grid">
                                        <div class="row">
                                            @if(count($products['products']) != 0)
                                                @foreach($products['products'] as $k => $x)
                                                    <div class="col-xl-3 col-md-6 col-grid-box">
                                                        <div class="product-box">
                                                            <div class="img-wrapper">
                                                                <div class="front">
                                                                    <a href="#"><img src="{{ URL::route('app.resources').'?src='.$x->photo }}" class="img-fluid blur-up lazyload bg-img" alt=""></a>
                                                                </div> 
                                                                <div class="cart-info cart-wrap"> 
                                                                    @if(Auth::user() && Auth::user()->type == 'user')
                                                                        <a href="javascript:void(0)" title="Add to Cart">
                                                                            <i class="ti-shopping-cart" aria-hidden="true"></i>
                                                                        </a> 
                                                                    @endif
                                                                    <a href="{{ URL::route('client.products',str_replace(' ','-',strtolower($x->name))) }}" title="Quick View">
                                                                        <i class="ti-eye" aria-hidden="true"></i>
                                                                    </a> 
                                                                    <a href="{{ URL::route('client.compare.add',$x->id) }}" title="Compare">
                                                                        <i class="ti-reload" aria-hidden="true"></i>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="product-detail">
                                                                <div>
                                                                    <div class="rating">
                                                                        <i class="fa fa-star"></i> 
                                                                        <i class="fa fa-star"></i> 
                                                                        <i class="fa fa-star"></i> 
                                                                        <i class="fa fa-star"></i> 
                                                                        <i class="fa fa-star"></i>
                                                                    </div> 
                                                                    <a href="{{ URL::route('client.products',str_replace(' ','-',strtolower($x->name))) }}"><h6>{{ $x->name }}</h6></a>
                                                                    <p>{{ $x->description }}</p>
                                                                    <h4>₱ {{ number_format($x->srp,2,'.',',') }}</h4> 
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            @else
                                                <center>
                                                    <br>
                                                    No products found !
                                                </center>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<style type="text/css">
    .cart-info{
        background-color: white !important;
    }
</style>
                      