@extends('frontend.components.layouts.main.index')
@section('title',$title)
@section('content')  
	<section class="cart-section section-b-space">
	    <div class="container">
        	@if($data && count($data) != 0)
		        <div class="row">
		            <div class="col-sm-12">
		                <table class="table cart-table table-responsive-xs">
		                    <thead>
			                    <tr class="table-head">  
			                        <th scope="col">Company Name</th>
			                        <th scope="col">Country</th>
			                        <th scope="col">Date Needed</th>
			                        <th scope="col">Order Sched</th>  
			                        <th scope="col">Status</th>   
			                        <th scope="col"></th>   
			                    </tr>
		                    </thead> 
		                    <tbody> 
		                    	@foreach($data as $x)
				                    <tr> 
				                    	<td>{{ $x->company_name }}</td> 
				                    	<td>{{ $x->country }}</td> 
				                    	<td>{{ $x->date_needed }}</td> 
				                    	<td>{{ $x->estimatedordersched }}</td> 
				                    	<td>{{ $x->quotation_status }}</td> 
				                    	<td>
				                    		<a href="{{ URL::route('client.quotation.view',$x->id) }}">
				                    			View
				                    		</a>
				                    	</td>
				                    </tr> 
				                @endforeach
		                </table> 
		            </div>
		        </div> 
            @else
            	<center>No items yet !</center>
            @endif
	    </div>
	</section> 
	@include('frontend.components.ads.landscape.index')
	<br><br> 
@endsection