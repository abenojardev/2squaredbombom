@if(isset($bread) && !is_null($bread))
    <div class="breadcrumb-section">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="page-title">
                        <h2> 
                            {{ $bread['title'] }}
                        </h2>
                    </div>
                </div>
                <div class="col-sm-6">
                    <nav aria-label="breadcrumb" class="theme-breadcrumb">
                        <ol class="breadcrumb">
                            @foreach($bread['links'] as $crumbs)
                                <li class="breadcrumb-item">
                                    <a href="{{ $crumbs['url'] }}">
                                        {{ $crumbs['title'] }}
                                    </a>
                                </li> 
                            @endforeach
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
@endif