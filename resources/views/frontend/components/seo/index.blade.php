@if(!is_null($seo))
	<meta name="description" content="{{ $seo->description }}">
	<meta name="keywords" content="{{ $seo->keywords }}">
	<meta name="author" content="{{ $seo->author }}"> 
@endif