<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="main-menu">
                <div class="menu-left"> 
                    <div class="brand-logo">
                        <a href="{{ URL::route('client.homepage') }}"> 
                        	<img src="{{ URL::asset('branding/logo.png') }}" class="img-fluid blur-up lazyload" alt=""> 
                        </a>
                    </div>
                </div>
                <div class="menu-right pull-right">
                    <div>
                        <nav id="main-nav">
                            <div class="toggle-nav">
                                <i class="fa fa-bars sidebar-bar"></i>
                            </div> 
                            <ul id="main-menu" class="sm pixelstrap sm-horizontal">
                                <li>
                                    <div class="mobile-back text-right">Back<i class="fa fa-angle-right pl-2" aria-hidden="true"></i></div>
                                </li>
                                <!-- <li>
                                    <a href="#">Home</a>
                                    <ul>
                                        <li>
                                            <a href="#">new demos</a>
                                            <ul>
                                                <li><a target="_blank"  href="tools.html">tools</a></li>
                                                <li><a target="_blank" href="game.html">game</a></li>
                                                <li><a target="_blank" href="gym-product.html">gym</a></li>
                                                <li><a target="_blank" href="marijuana.html">marijuana</a></li>
                                                <li><a target="_blank" href="left_sidebar-demo.html">left sidebar</a></li>
                                                <li><a target="_blank" href="jewellery.html">jewellery</a></li>
                                                <li><a target="_blank" href="pets.html">pets</a></li>
                                                <li><a target="_blank" href="metro.html">metro</a></li>
                                                <li><a target="_blank" href="video-slider.html">video slider</a></li>
                                            </ul>
                                        </li> 
                                    </ul>
                                </li>  
                               <li class="mega" id="hover-cls">
                                    <a href="#">
                                        Become a Partner
                                        <div class="lable-nav">Join Now</div>    
                                    </a> 
                                </li>   -->
                                <li>
                                    <a href="{{ URL::route('client.homepage') }}">Home</a> 
                                </li>
                                <li>
                                    <a href="{{ URL::route('client.homepage') }}">About Us</a> 
                                </li>
                                <li class="mega" id="hover-cls">
                                    <a href="{{ URL::route('client.compare') }}">
                                        Compare 
                                        @if(session('compare.items') !== null)
                                            <div class="lable-nav">{{ count(session('compare.items')) }} Products</div>    
                                        @endif
                                    </a> 
                                </li>  
                                <li class="mega" id="hover-cls">
                                    <a href="{{ URL::route('client.cart') }}">
                                        View Cart
                                        @if(session('cart.items') !== null)
                                            <div class="lable-nav">{{ count(session('cart.items')) }}</div>    
                                        @endif 
                                    </a> 
                                </li>  
                                <li>
                                    @if(Auth::user() && Auth::user()->type == 'user') 
                                        <a href="#">Account</a> 
                                    @else
                                        <a href="#">Join Us</a> 
                                    @endif
                                    <ul>
                                        @if(Auth::user() && Auth::user()->type == 'user')   
                                            <li><a  href="{{ URL::route('client.login') }}">Quotations</a></li> 
                                            <li><a  href="{{ URL::route('client.logout') }}">Logout</a></li>
                                        @else
                                            <li><a  href="{{ URL::route('client.login') }}">Login</a></li>  
                                            <li><a  href="{{ URL::route('client.login') }}">Register</a></li>  
                                        @endif 
                                    </ul> 
                                </li>
                            </ul>
                        </nav>
                    </div> 
                </div>
            </div>
        </div>
    </div>
</div>