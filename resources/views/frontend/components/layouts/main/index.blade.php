<!DOCTYPE html>
<html lang="en"> 
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">

    <title>@yield('title') | {{ env('APP_NAME') }}</title> 
    <link rel="icon" href="{{ URL::asset('branding/icon.png') }}">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet"> 
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('frontend/css/fontawesome.css') }}"> 
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('frontend/css/slick.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('frontend/css/slick-theme.css') }}"> 
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('frontend/css/animate.css') }}"> 
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('frontend/css/themify-icons.css') }}"> 
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('frontend/css/bootstrap.css') }}"> 
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('frontend/css/color1.css') }}" media="screen" id="color">

    @include('frontend.components.seo.index',[ 'seo' => isset($seo) ? $seo : null ])
</head>

<body>
 
    <header>
        <div class="mobile-fix-option"></div>
        @include('frontend.components.topbar.index')
        @include('frontend.components.navbar.index') 
    </header> 

    @include('frontend.components.breadcrumbs.index',[ 'bread' => isset($bread) ? $bread : null ]) 

    @yield('content')

    @include('frontend.components.backtotop.index') 
    @include('frontend.components.footer.index') 
    @include('frontend.components.backtotop.index') 
  
	<script src="{{ URL::asset('frontend/js/jquery-3.3.1.min.js') }}"></script>  
	<script src="{{ URL::asset('frontend/js/menu.js') }}"></script> 
	<script src="{{ URL::asset('frontend/js/lazysizes.min.js') }}"></script> 
	<script src="{{ URL::asset('frontend/js/popper.min.js') }}"></script> 
	<script src="{{ URL::asset('frontend/js/slick.js') }}"></script> 
	<script src="{{ URL::asset('frontend/js/bootstrap.js') }}"></script> 
	<script src="{{ URL::asset('frontend/js/bootstrap-notify.min.js') }}"></script> 
	<script src="{{ URL::asset('frontend/js/script.js') }}"></script>  
</body> 
</html>

