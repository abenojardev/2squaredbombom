<div class="top-header">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="header-contact">
                    <ul>
                        <li>Welcome to Our store Multikart</li>
                        <li><i class="fa fa-phone" aria-hidden="true"></i>Call Us:  123 - 456 - 7890</li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-6 text-right">
                <ul class="header-dropdown">
                    <li class="mobile-wishlist">
                        @if(Auth::user() && Auth::user()->type == 'user')
                            Welcome {{ Auth::user()->name }}
                        @else
                            Login to your account
                        @endif 
                    </li>
                    <li class="onhover-dropdown mobile-account">
                        <i class="fa fa-user" aria-hidden="true"></i> My Account
                        <ul class="onhover-show-div">

                            @if(Auth::user() && Auth::user()->type == 'user') 
                                <li>
                                    <a href="{{ URL::route('client.logout') }}" data-lng="es">
                                        Logout
                                    </a>
                                </li>
                            @else
                                <li>
                                    <a href="{{ URL::route('client.login') }}" data-lng="en">
                                        Login
                                    </a>
                                </li>
                                <li>
                                    <a href="{{ URL::route('client.login') }}" data-lng="en">
                                        Register
                                    </a>
                                </li> 

                            @endif
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>