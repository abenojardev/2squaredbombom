<section class="p-0">
    <div class="slide-1 home-slider">
        <div>
            <div class="home text-left p-left">
                <img src="{{ URL::asset('frontend/images/home-banner/14.jpg') }}" alt="" class="bg-img blur-up lazyload">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <div class="slider-contain">
                                <div>
                                    <h4>get the best</h4>
                                    <h1>Electrical</h1><a href="#" class="btn btn-outline btn-classic">shop now</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <div class="home text-left p-left">
                <img src="{{ URL::asset('frontend/images/home-banner/15.jpg') }}" alt="" class="bg-img blur-up lazyload">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <div class="slider-contain">
                                <div>
                                    <h4>shop for tools</h4>
                                    <h1>Communication</h1><a href="#" class="btn btn-outline btn-classic">shop now</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <div class="home text-left p-left">
                <img src="{{ URL::asset('frontend/images/home-banner/16.jpg') }}" alt="" class="bg-img blur-up lazyload">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <div class="slider-contain">
                                <div>
                                    <h4>see new products</h4>
                                    <h1>Info tech</h1><a href="#" class="btn btn-outline btn-classic">shop now</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>