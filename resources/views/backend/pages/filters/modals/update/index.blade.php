@foreach($data as $x)
    <div class="modal fade" id="update_{{ $x->id }}" tabindex="-1" role="dialog" aria-labelledby="create" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Update</h5><button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                </div>
                <form action="{{ URL::route('app.products.filters.update',$x->id) }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">  
                        <div class="form-group mb-4">
                            <label class="active">Filter Title</label>
                            <input class="form-control" type="text" name="title" required value="{{ $x->title }}"> 
                        </div>  
                    </div>
                    <div class="modal-footer">
                        <button class="btn" type="button" data-dismiss="modal">Close</button>
                        <button class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endforeach