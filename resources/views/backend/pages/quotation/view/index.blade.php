@php $active = 'quotations' @endphp 
@extends('backend.layouts.main.index')
@section('title','Quotations Details')
@section('content') 
        <div class="page-content fade-in-up">
        <!-- BEGIN: Page heading-->
        <div class="page-heading">
            <div class="page-breadcrumb">
                <h1 class="page-title page-title-sep">Quotations</h1> 
            </div>
        </div>
        <div>
            <div class="card">
                <div class="card-body invoice px-sm-5">
                    <div class="row">
                        <div class="col-md-6">
                            <h1 class="text-muted font-40 mb-5">#{{ $data->id }}</h1>
                        </div>
                        <div class="col-md-6 text-left text-sm-right">
                            <h2 class="text-danger mb-3">{{ $data->company_name }}</h2>
                            <div class="font-15 text-muted">
                                <div>{{ $data->country }}</div>
                                <div>Date Need : {{ $data->date_needed}}</div>
                                <div>Order Sched : {{ $data->estimatedordersched }}</div>
                            </div>
                        </div>
                    </div>
                    <hr class="my-5">
                    <div class="row mb-5">
                        <div class="col-md-6 mb-4">
                            <h5 class="text-primary mb-3">INVOICE TO</h5>
                            <div class="font-15 text-muted">
                                <div>{{ $data->company_name }}</div> 
                                <div>{{ $data->country }}</div> 
                                <div>{{ App\Models\User::find($data->user_id)->name }}</div>
                                <div>{{ App\Models\User::find($data->user_id)->email }}</div>
                            </div>
                        </div>
                        <div class="col-md-6 mb-4 text-left text-sm-right">
                            <h5 class="mb-3">INVOICE INFO</h5>
                            <div class="ml-auto" style="width: 260px;">
                                <div class="row align-items-center mb-2">
                                    <div class="col-6 text-muted">Invoice N.</div>
                                    <div class="col-6 font-weight-strong">null</div>
                                </div>
                                <div class="row align-items-center mb-2">
                                    <div class="col-6 text-muted">Invoice Date</div>
                                    <div class="col-6 font-weight-strong">null</div>
                                </div>
                                <div class="row align-items-center">
                                    <div class="col-6 text-muted">Issue Date</div>
                                    <div class="col-6 font-weight-strong">null</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-fullwidth-block mx-sm-0">
                        <div class="table-responsive">
                            <form method="post" action="{{ URL::route('app.quotations.quote',$data->id) }}">
                                @csrf
                                <table class="table invoice-table">
                                    <thead class="thead-light">
                                        <tr>
                                            <th>Product</th>
                                            <th>Custom Specs</th>
                                            <th>QTY</th> 
                                            <th class="text-right">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php $total = 0; @endphp
                                        @foreach($products as $x)
                                            @if(!is_null($x->price))
                                                @php $total = $total + $x->price; @endphp
                                                @php $price = $x->price; @endphp
                                            @else
                                                @php $price = 0; @endphp
                                            @endif
                                            <tr>
                                                <td>
                                                    <div class="font-weight-bold font-16">
                                                        {{ $x->details->name }}
                                                    </div>
                                                    <div class="d-none d-sm-block">
                                                        {{ $x->details->part_number }}
                                                    </div>
                                                </td>
                                                <td>none</td>
                                                <td>
                                                    {{ $x->qty }}
                                                </td> 
                                                <td class="text-right" width="20%">
                                                    <input type="number" name="{{ $x->id }}" required value="{{ $price }}" class="form-control">
                                                </td>
                                            </tr> 
                                        @endforeach
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th colspan="5"></th>
                                        </tr>
                                    </tfoot>
                                </table> 
                                @if(session('success')) 
                                    <p>
                                        {{ session('success') }}
                                    </p>
                                @elseif(session('error'))
                                    <p class="error">
                                        {{ session('error') }}
                                    </p>
                                @endif
                                <button class="btn btn-success">Save Quotation</button>
                            </form>
                        </div>
                    </div>
                    <div class="d-flex justify-content-end">
                        <div class="text-right mr-3" style="width:300px;">
                            <!-- <div class="row mb-2">
                                <div class="col-6">Subtotal Price</div>
                                <div class="col-6">$2040</div>
                            </div>  -->
                            <div class="row font-weight-strong font-20 align-items-center text-primary">
                                <div class="col-4">Total:</div>
                                <div class="col-8">
                                    <div class="h3 mb-0">P {{ number_format($total,2,'.',',') }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- END: Page content-->
    </div>
@endsection 
@section('extraJs')
    <script type="text/javascript" src="{{ URL::asset('assets/libraries/angular.min.js') }}"></script>  
@endsection