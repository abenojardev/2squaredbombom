@php $active = 'quotations' @endphp 
@extends('backend.layouts.main.index')
@section('title','Quotations')
@section('content')
    <div class="page-content fade-in-up"> 
        <div class="page-heading">
            <div class="page-breadcrumb">
                <h1 class="page-title">Quotations</h1>
            </div>  
        </div>
        <div>
            <div class="card">
                <div class="card-body">
                    <h5 class="box-title"></h5>
                    <div class="table-responsive">
                        <table class="table table-bordered w-100" id="dt-base">
                            <thead class="thead-light">
                                <tr> 
                                    <th width="5%">Quote #</th>  
                                    <th>Customer</th>
                                    <th>Company Name</th>
                                    <th>Country</th>
                                    <th>Date Need</th>
                                    <th>EOS</th>
                                    <th>Status</th>  
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>  
                                @foreach($data as $x)
                                    <tr>  
                                        <td>{{ $x->id }}</td>
                                        <td>{{ App\Models\User::find($x->user_id)->name }}</td>
                                        <td>{{ $x->company_name }}</td>
                                        <td>{{ $x->country }}</td>
                                        <td>{{ $x->date_needed }}</td>
                                        <td>{{ $x->estimatedordersched }}</td>
                                        <td>
                                            @if($x->quotation_status == 'pending')
                                                <span class="badge badge-pill text-white badge-warning">PENDING</span>
                                            @else
                                                <span class="badge badge-pill text-white badge-success">CLOSED</span>
                                            @endif
                                        </td> 
                                        <td>
                                            <a href="{{ URL::route('app.quotations.view',$x->id) }}">Send Quote</a>
                                        </td>
                                    </tr> 
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('extraCss')
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/vendors/DataTables/datatables.min.css') }}">
@endsection
@section('extraJs')
    <script type="text/javascript" src="{{ URL::asset('assets/vendors/DataTables/datatables.min.js') }}"></script> 
    <script src="{{ URL::asset('assets/js/dt.js') }}"></script>   
@endsection