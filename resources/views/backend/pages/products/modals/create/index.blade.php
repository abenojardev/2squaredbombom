<div class="modal fade" id="create" tabindex="-1" role="dialog" aria-labelledby="create" aria-hidden="true">
    <div class="modal-dialog" role="document" ng-app="app" ng-controller="category">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Create</h5><button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <form action="{{ URL::route('app.products.create') }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">  
                    <div class="form-group mb-4">
                        <label class="active">Select Category</label> 
                        <img ng-if="loading" src="{{ URL::asset('assets/img/loader.gif') }}" width="5%">
                        <select class="form-control" type="text" name="category" required ng-model="categoryx" ng-change="getSubCategories()">
                            @foreach($category as $x)
                                <option value="{{ $x->id }}">{{ $x->title }}</option>
                            @endforeach
                        </select>
                        <select ng-repeat="x in sub" class="form-control" type="text" name="subcategory" required ng-model="subcategory" ng-change="getChild(subcategory)"> 
                            <option ng-repeat="y in x.data" ng-reflect-ng-value="<< y.id >>" value="<< y.id >>"><< y.title >></option> 
                        </select> 
                        <input type="hidden" name="category_id" value="<< main_category >>">
                    </div>      
                    <div ng-if="form">
                        <div class="form-group mb-4">
                            <label class="active">Product Name</label>
                            <input class="form-control" type="text" name="name" required>
                        </div>  
                        <div class="form-group mb-4">
                            <label class="active">Part Number</label>
                            <input class="form-control" type="text" name="partnumber" required>
                        </div>  
                        <div class="form-group mb-4">
                            <label class="active">SRP</label>
                            <input class="form-control" type="text" name="srp" required>
                        </div>  
                        <div class="form-group mb-4">
                            <label class="active">Description</label>
                            <textarea class="form-control" type="text" name="description" required rows="5"></textarea>
                        </div>  
                        <div class="form-group mb-4">
                            <label class="active">Availability</label>
                            <select class="form-control" type="text" name="availability" required>
                                <option value="1">Available</option>
                                <option value="0">Not-Available</option>
                            </select>
                        </div>  
                        <div class="form-group mb-4">
                            <label class="active">Photos</label>
                            <input class="form-control" type="file" name="photo" required>
                        </div>  
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn" type="button" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary" ng-if="form">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript" src="{{ URL::asset('libraries/angular.min.js') }}"></script> 
<script type="text/javascript">
        angular.module('app',[], function($interpolateProvider) {
            $interpolateProvider.startSymbol('<<');
            $interpolateProvider.endSymbol('>>');
        }).controller('category',function($scope,$interval,$http,$timeout){

            $scope.form = false;
            $scope.loading = false;
            $scope.category = 'Choose Category';
            $scope.url = '{{ URL::route("api.products.categories") }}';
            $scope.main_category = null;
            $scope.subcategory = null;
            $scope.sub = [];
            $scope.id = 1;

            $scope.getSubCategories = () => {
                $scope.form = false;
                $scope.loading = true;
                $scope.main_category = $scope.category;
                $scope.sub = [];

                $http.get($scope.url+'?id='+$scope.categoryx).then(function(data){
                    if (data.data.data.length <= 0) {
                        $scope.subcategory = $scope.categoryx;
                        console.log($scope.subcategory);
                        $scope.form = true;
                    } else { 
                        $scope.sub.push({
                            id : $scope.id,
                            data : data.data.data
                        }); 
                    }
                    $scope.id = $scope.id + 1;
                    $scope.loading = false;
                });
            }

            $scope.getChild = (subcategory) => { 
                $scope.loading = true; 
                $scope.main_category = subcategory;

                $http.get($scope.url+'?id='+subcategory).then(function(data){
                    if (data.data.data.length <= 0) {
                        console.log(subcategory);
                        $scope.form = true;
                    } else { 
                        $scope.sub.push({
                            id : $scope.id,
                            data : data.data.data
                        }); 
                    }
                    $scope.id = $scope.id + 1;
                    $scope.loading = false;
                });
            }
        });
</script>
























