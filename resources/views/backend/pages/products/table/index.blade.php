@php $active = 'products' @endphp
@php $subactive = 'products' @endphp
@extends('backend.layouts.main.index')
@section('title','Products')
@section('content')
    <div class="page-content fade-in-up"> 
        <div class="page-heading">
            <div class="page-breadcrumb">
                <h1 class="page-title">Products</h1>
            </div> 
            @if(Auth::user()->type != 'supplier')
                <div>
                    <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#create"> 
                        <span class="ft-plus"></span> Create
                    </button>
                </div> 
            @else
                <div>
                    <a class="btn btn-primary text-white" href="{{ URL::route('app.products.search') }}"> 
                        <span class="ft-search"></span> Search Products to sell
                    </a>
                </div> 
            @endif
        </div>
        <div>
            <div class="card">
                <div class="card-body">
                    <h5 class="box-title"></h5>
                    <div class="table-responsive">
                        <table class="table table-bordered w-100" id="dt-base">
                            <thead class="thead-light">
                                <tr> 
                                    <th width="5%"></th>  
                                    <th>Name</th>
                                    <th>Category</th>
                                    <th>Part Number</th>
                                    <th>SRP</th>
                                    <th>Availability</th> 
                                    @if(Auth::user()->type != 'supplier')
                                        <th></th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>  
                                @foreach($data as $x)
                                    <tr> 
                                        <td><img src="{{ URL::route('app.resources').'?src='.$x->photo }}"></td>
                                        <td>
                                            <a href="{{ URL::route('app.products.view',$x->id) }}">
                                                {{ $x->name }}
                                            </a>
                                        </td>  
                                        <td>{{ App\Models\ProductCategories::find($x->category_id)->title }}</td>
                                        <td>
                                            {{ $x->part_number }}
                                        </td>
                                        <td>
                                            P {{ $x->srp }}
                                        </td>
                                        <td>
                                            @if($x->is_available == 1)
                                                <span class="badge badge-pill badge-success">Available</span>
                                            @else
                                                <span class="badge badge-pill badge-danger">Unvailable</span>
                                            @endif
                                        </td>
                                        @if(Auth::user()->type != 'supplier')
                                            <td>
                                                <div class="mb-4"> 
                                                    <div class="btn-group" role="group">
                                                        <button class="btn btn-light dropdown-toggle" id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Actions</button>
                                                        <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                                            <a class="dropdown-item" data-toggle="modal" data-target="#update_{{ $x->id }}">Update</a>
                                                            <a class="dropdown-item" href="{{ URL::route('app.products.delete',$x->id) }}">Delete</a>
                                                        </div>
                                                    </div> 
                                                </div>
                                            </td> 
                                        @endif
                                    </tr> 
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('backend.pages.products.modals.create.index')
    @include('backend.pages.products.modals.update.index',['data' => $data])
@endsection
@section('extraCss')
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/vendors/DataTables/datatables.min.css') }}">
@endsection
@section('extraJs')
    <script type="text/javascript" src="{{ URL::asset('assets/vendors/DataTables/datatables.min.js') }}"></script> 
    <script src="{{ URL::asset('assets/js/dt.js') }}"></script>   
@endsection