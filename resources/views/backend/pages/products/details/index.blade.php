@php $active = 'products' @endphp
@php $subactive = 'products' @endphp
@extends('backend.layouts.main.index')
@section('title','Products')
@section('content')
    <div class="page-content fade-in-up" ng-app="app" ng-controller="product"> 
        <div>
            <div class="bg-white pt-4 px-5" style="margin: -30px -30px 40px">
                <div class="flexbox mb-4 mt-3">
                    <div class="media">
                        <span class="position-relative d-inline-block mr-4">
                            <img class="rounded-circle" src="{{ URL::route('app.resources').'?src='.$data->photo }}" alt="image" width="100" />     
                        </span>
                        <div class="media-body">
                            <div class="h4">{{ $data->name }}</div>
                            <div class="text-muted">
                                {{ $data->description }}
                            </div>
                        </div>
                    </div>
                </div>
                <ul class="nav line-tabs m-0"> 
                    <li class="nav-item show active">
                        <a class="nav-link" data-toggle="tab" href="#tab-2"><i class="ti-pulse nav-tabs-icon"></i>Variations</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#tab-3"><i class="ti-settings nav-tabs-icon"></i>Filters</a>
                    </li> 
                </ul>
            </div>
            <div class="tab-content mt-5"> 
                <div class="tab-pane  fade show active" id="tab-2">
                    @if(Auth::user()->type != 'supplier')
                        <form action="{{ URL::route('app.products.specs.update',$data->id) }}" method="post">
                            @csrf
                            <input type="hidden" name="specs" value="<< temp_specs >>">
                            <a class="btn btn-light waves-effect" type="reset" data-toggle="modal" data-target="#createVariation">Add Variation</a>
                            <button class="btn btn-success waves-effect">Save Variation</button>
                        </form>
                    @else
                        <form action="{{ URL::route('app.products.addtosupplier',$data->id) }}" method="post">
                            @csrf 
                            <button class="btn btn-success waves-effect">Add to my products</button>
                        </form>
                    @endif
                    <br><br>
                    @if(session('success'))
                        <span>{{ session('success') }}</span><br><br>
                    @endif
                    <div class="card" ng-repeat="(key,x) in temp_specs">
                        <div class="card-body">
                            <div class="d-flex justify-content-between mb-4">
                                <div>
                                    <h5 class="box-title mb-2"><< x.name >></h5> 
                                </div> 
                                @if(Auth::user()->type != 'supplier')
                                    <a class="text-muted" href="#">
                                        <div class="text-muted font-13 ml-3" style="min-width: 80px;"> 
                                            <div>
                                                <a class="btn btn-sm"><i class="ti-plus" ng-click="setActiveVariation(key)" data-toggle="modal" data-target="#createVariationItem"></i></a>
                                                <a class="btn btn-sm"><i class="ti-trash" ng-click="deleteVariation(key)"></i></a>
                                            </div>
                                        </div>
                                    </a> 
                                @endif
                            </div>
                            <hr>
                            <ul class="list-unstyled media-list-divider"> 
                                <li class="media py-3" ng-repeat="(k,y) in x.items"> 
                                    <div class="media-body">
                                        <div class="d-flex justify-content-between mb-2">
                                            <h5><< y.name >></h5>  
                                            @if(Auth::user()->type != 'supplier')
                                                <div class="text-muted font-13 ml-3" style="min-width: 80px;"> 
                                                    <div> 
                                                        <a class="btn btn-sm"><i class="ti-trash" ng-click="deleteVariationItem(key,k)"></i></a>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                        <p class="text-muted">
                                            <b>PartNumber</b> : << y.partnumber >> <br>
                                            <b>SRP</b> : P << y.srp >>
                                        </p>
                                    </div>
                                </li> 
                            </ul>
                        </div>
                    </div> 
                </div>
                <div class="tab-pane fade" id="tab-3">
                    <div class="card">
                        <div class="card-body">
                            <h4 class="box-title">Product Filters</h4>  
                            @if($filters)
                                <form action="{{ URL::route('app.products.filter.update',$data->id) }}" method="post">
                                    @csrf
                                    <div class="col-12"> 
                                        @foreach($filters as $key => $x) 
                                            <h6>{{ $x->title }}</h6>
                                            @foreach($x->items as $k => $y) 
                                                <label class="radio radio-primary mt-3">  
                                                    <input type="radio" value="{{ $y->id }}" name="{{ $x->id }}" @if(count($data->filters) != 0 && $data->filters[$x->id] == $y->id) checked="true" @endif>
                                                    <span>{{ $y->type }}</span>
                                                </label>
                                            @endforeach
                                            <br>
                                        @endforeach
                                    </div> 
                                    <br><hr>
                                    @if(Auth::user()->type != 'supplier')
                                        <div class="form-group">
                                            <button class="btn btn-primary mr-2 waves-effect waves-light">Save Filters</button>
                                        </div>
                                    @endif
                                </form>
                            @else
                                @if(Auth::user()->type != 'supplier')
                                    <center>
                                        No filters for this category. To add filter for this category <a href="{{ URL::route('app.products.filters',$data->category_id) }}">click here</a>.
                                    </center>
                                @endif
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="createVariation" tabindex="-1" role="dialog" aria-labelledby="create" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Create</h5><button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    </div> 
                    <div class="modal-body">     
                        <div>
                            <div class="form-group mb-4">
                                <label class="active">Variation Type</label>
                                <p class="error"><< create_variation_error >></p>
                                <input class="form-control" type="text" ng-model="create_variation_type">
                            </div>   
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn" type="button" id="closeBtn" data-dismiss="modal">Close</button>
                        <button class="btn btn-primary" ng-click="saveVariation()">Save</button>
                    </div> 
                </div>
            </div>
        </div>

        <div class="modal fade" id="createVariationItem" tabindex="-1" role="dialog" aria-labelledby="create" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Create</h5><button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    </div> 
                    <div class="modal-body">     
                        <div>
                            <div class="form-group mb-4">
                                <label class="active">Variation Specs</label>
                                <p class="error"><< create_variation_item_error >></p>
                                <input class="form-control" type="text" ng-model="create_variation_item_type">
                            </div>
                            <div class="form-group mb-4">
                                <label class="active">PartNumber</label> 
                                <input class="form-control" type="text" ng-model="create_variation_item_partnumber">
                            </div>   
                            <div class="form-group mb-4">
                                <label class="active">SRP</label> 
                                <input class="form-control" type="number" ng-model="create_variation_item_srp">
                            </div>   
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn" type="button" id="closeBtnItem" data-dismiss="modal">Close</button>
                        <button class="btn btn-primary" ng-click="saveVariationItem()">Save</button>
                    </div> 
                </div>
            </div>
        </div>

    </div>
@endsection
@section('extraCss')
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/vendors/DataTables/datatables.min.css') }}">
@endsection
@section('extraJs')
<script type="text/javascript" src="{{ URL::asset('libraries/angular.min.js') }}"></script> 
    <script type="text/javascript">
        angular.module('app',[], function($interpolateProvider) {
            $interpolateProvider.startSymbol('<<');
            $interpolateProvider.endSymbol('>>');
        }).controller('product',function($scope,$interval,$http,$timeout){ 
            $scope.temp_specs = {!! json_encode($data->specs,JSON_HEX_APOS) !!}; 
            // $scope.temp_specs =  json_encode(json_decode($data->specs),JSON_HEX_APOS) ; 


            $scope.saveVariation = () => {
                if($scope.create_variation_type == '' || $scope.create_variation_type == null){
                    $scope.create_variation_error = "Please fillup this field !";
                } else {
                    $scope.create_variation_error = null;
                    $scope.temp_specs.push({ 
                        name : $scope.create_variation_type,
                        items : []
                    });
                    $scope.create_variation_type = null;
                    document.getElementById('closeBtn').click();
                }
            };

            $scope.deleteVariation = (k) => {
                if(window.confirm('Are you sure you want to delete this ?')) {
                    $scope.temp_specs.splice(k,1);
                }
            };

            $scope.deleteVariationItem = (key,k) => {
                if(window.confirm('Are you sure you want to delete this ?')) {
                    $scope.temp_specs[key].items.splice(k,1);
                }
            };

            $scope.setActiveVariation = (key) => {
                $scope.activeVariation = key; 
            };

            $scope.saveVariationItem = () => {
                if($scope.create_variation_item_type == '' || $scope.create_variation_item_type == null ||
                    $scope.create_variation_item_partnumber == '' || $scope.create_variation_item_partnumber == null ||
                    $scope.create_variation_item_srp == '' || $scope.create_variation_item_srp == null){

                    $scope.create_variation_item_error = "Please fillup this field !";

                } else {
                    $scope.create_variation_item_error = null;
                    $scope.temp_specs[$scope.activeVariation].items.push({ 
                        name : $scope.create_variation_item_type,
                        partnumber : $scope.create_variation_item_partnumber,
                        srp : $scope.create_variation_item_srp 
                    });
                    $scope.create_variation_item_type = null;
                    $scope.create_variation_item_partnumber = null;
                    $scope.create_variation_item_srp = null;
                    document.getElementById('closeBtnItem').click();
                }
            };
        });
    </script>
    <script type="text/javascript" src="{{ URL::asset('assets/vendors/DataTables/datatables.min.js') }}"></script> 
    <script src="{{ URL::asset('assets/js/dt.js') }}"></script>   
@endsection







