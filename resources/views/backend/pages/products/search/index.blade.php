@php $active = 'search' @endphp 
@extends('backend.layouts.main.index')
@section('title','Search Products')
@section('content') 
    <div class="page-content fade-in-up">
        <!-- BEGIN: Page heading-->
        <div class="page-heading">
            <div class="page-breadcrumb">
                <h1 class="page-title page-title-sep">Search Product</h1> 
            </div>
        </div>
        <div class="input-group-icon input-group-icon-left input-group-lg mb-4">
            <span class="input-icon input-icon-left"><i class="ti-search"></i></span>
            <form method="get">
                <input class="form-control font-weight-light border-0" type="text" name="q" placeholder="Search ..." style="box-shadow:0 3px 6px rgba(10,16,20,.15);">
            </form>
        </div>
        <div class="card">
            <div class="card-body"> 
                <div class="tab-pane fade active show" id="search-2">
                    @if(!is_null($q))
                        <h4 class="mb-5">{{ count($data) }} products found for: <span class="text-primary">“{{ $q }}”</span></h4>  
                    @endif
                    <ul class="list-unstyled">
                        @if($data)
                            @foreach($data as $x)
                                <li class="media py-3">
                                    <img class="mr-4" src="{{ URL::route('app.resources').'?src='.$x->photo }}" alt="image" width="80">
                                    <div class="media-body">
                                        <h5><a href="{{ URL::route('app.products.view',$x->id) }}">{{ $x->name }}</a></h5>
                                        <p class="mb-1">{{ $x->description }}</p>
                                        <div class="text-muted font-13">
                                            <span class="font-weight-bold text-danger">
                                                {{ App\Models\ProductCategories::find($x->category_id)->title }}
                                            </span>
                                            <span class="mx-2">•</span>
                                            <span>{{ $x->part_number }}</span>
                                        </div>
                                    </div>
                                </li> 
                            @endforeach 
                        @else
                            <center>Empty search bin...</center>
                        @endif
                    </ul> 
                </div>
            </div>
        </div>
    </div>
@endsection 