@php $active = 'accounts' @endphp
@php $subactive = $type @endphp
@extends('backend.layouts.main.index')
@section('title','Accounts')
@section('content')
    <div class="page-content fade-in-up"> 
        <div class="page-heading">
            <div class="page-breadcrumb">
                <h1 class="page-title">{{ ucfirst($type) }}</h1>
            </div>  
        </div>
        <div>
            <div class="card">
                <div class="card-body">
                    <h5 class="box-title"></h5>
                    <div class="table-responsive">
                        <table class="table table-bordered w-100" id="dt-base">
                            <thead class="thead-light">
                                <tr> 
                                    <th width="5%"></th>  
                                    <th>Name</th>
                                    <th>Email Address</th>
                                    @if($type != 'users')
                                        <th>Company Documents</th>
                                    @endif
                                    <th>Email Verified</th>
                                    <th>Status</th>
                                    <th>Created at</th> 
                                    @if($type != 'users')
                                        <th></th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>  
                                @foreach($data as $x)
                                    <tr> 
                                        <td><img src="{{ URL::asset('assets/img/default.jpeg') }}"></td> 
                                        <td>{{ $x->name }}</td>
                                        <td>{{ $x->email }}</td>
                                        @if($type != 'users')
                                            <td>null</td>
                                        @endif
                                        <td>
                                            @if(!is_null($x->email_verified_at))
                                                <span class="badge badge-pill badge-success">Verified</span>
                                            @else
                                                <span class="badge badge-pill badge-danger">Not Verified</span>
                                            @endif
                                        </td>
                                        <td>
                                            @if($x->status == 1)
                                                <span class="badge badge-pill badge-success">Active</span>
                                            @else
                                                <span class="badge badge-pill badge-danger">Disabled</span>
                                            @endif
                                        </td>
                                        <td>{{ $x->created_at->diffForHumans() }}</td>
                                        @if($type != 'users')
                                            <td> 
                                                @if($x->status == 1)
                                                    <a class="btn btn-warning btn-sm text-white" href="{{ URL::route('app.accounts.disable',$x->id) }}">
                                                        <i class="ti-lock"></i> Disable Account
                                                    </a>
                                                @else
                                                    <a class="btn btn-primary btn-sm text-white" href="{{ URL::route('app.accounts.enable',$x->id) }}">
                                                        <i class="ti-user"></i> Enable Account
                                                    </a>
                                                @endif 
                                            </td> 
                                        @endif
                                    </tr> 
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div> 
@endsection
@section('extraCss')
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/vendors/DataTables/datatables.min.css') }}">
@endsection
@section('extraJs')
    <script type="text/javascript" src="{{ URL::asset('assets/vendors/DataTables/datatables.min.js') }}"></script> 
    <script src="{{ URL::asset('assets/js/dt.js') }}"></script>   
@endsection