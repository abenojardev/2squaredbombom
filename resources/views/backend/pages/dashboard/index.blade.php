@php $active = 'dashboard' @endphp
@extends('backend.layouts.main.index')
@section('title','Dashboard')
@section('content')
    <div class="page-content fade-in-up"> 
        <div class="page-heading">
            <div class="page-breadcrumb">
                <h1 class="page-title">Dashboard</h1>
            </div> 
        </div>
        <div>
            <div class="row">
                <div class="col-3">
                    <div class="card">
                        <div class="card-body">
                            <h6 class="mb-3 font-15 text-muted font-weight-normal">Products</h6>
                            <div class="h2 mb-0 font-weight-normal">310</div><i class="ft-shopping-cart data-widget-icon"></i>
                        </div>
                    </div>
                </div>
                <div class="col-3">
                    <div class="card">
                        <div class="card-body">
                            <h6 class="mb-3 font-15 text-muted font-weight-normal">Categories</h6>
                            <div class="h2 mb-0 font-weight-normal">7450</div><i class="ft-list data-widget-icon"></i>
                        </div>
                    </div>
                </div>
                <div class="col-3">
                    <div class="card">
                        <div class="card-body">
                            <h6 class="mb-3 font-15 text-muted font-weight-normal">Accounts</h6>
                            <div class="h2 mb-0 font-weight-normal">40</div><i class="ft-user data-widget-icon"></i>
                        </div>
                    </div>
                </div>
                <div class="col-3">
                    <div class="card">
                        <div class="card-body">
                            <h6 class="mb-3 font-15 text-muted font-weight-normal">Quotations</h6>
                            <div class="h2 mb-0 font-weight-normal">8</div><i class="ft-file-text data-widget-icon"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="box-title">Sales Distribution</h5>
                            <div class="row">
                                <div class="col-sm-6"><div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"><div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div></div><div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:200%;height:200%;left:0; top:0"></div></div></div><canvas id="pie_chart_1" style="height: 188px; display: block; width: 270px;" width="810" height="564" class="chartjs-render-monitor"></canvas></div>
                                <div class="col-sm-6">
                                    <div class="mt-4">
                                        <div class="flexbox mb-3">
                                            <div><span class="badge-point badge-primary rounded-0 mr-2"></span><span>In-Store Sales</span></div><span class="h6 mb-0 font-16">$25976</span>
                                        </div>
                                        <div class="flexbox mb-3">
                                            <div><span class="badge-point badge-info rounded-0 mr-2"></span><span>Online Sales</span></div><span class="h6 mb-0 font-16">$6494</span>
                                        </div>
                                        <div class="flexbox mb-2">
                                            <div><span class="badge-point badge-danger rounded-0 mr-2"></span><span>Sales Rate</span></div><span class="h6 mb-0 font-16">+12%</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <h5 class="box-title">Rates</h5>
                            <div>
                                <div class="h6">Average profit</div>
                                <div class="media align-items-center mb-4">
                                    <div class="media-body">
                                        <div class="progress" style="height: 10px; box-shadow: 0 4px 4px rgba(0,0,0,.08);">
                                            <div class="progress-bar bg-info" role="progressbar" style="width: 80%;" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </div><span class="h5 mb-0 ml-4">80%</span>
                                </div>
                                <div class="h6">Affiliate Rate</div>
                                <div class="media align-items-center mb-4">
                                    <div class="media-body">
                                        <div class="progress" style="height: 10px; box-shadow: 0 4px 4px rgba(0,0,0,.08);">
                                            <div class="progress-bar" role="progressbar" style="width: 40%;" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </div><span class="h5 mb-0 ml-4">40%</span>
                                </div>
                                <div class="h6">Avg. order percentage</div>
                                <div class="media align-items-center mb-4">
                                    <div class="media-body">
                                        <div class="progress" style="height: 10px; box-shadow: 0 4px 4px rgba(0,0,0,.08);">
                                            <div class="progress-bar" role="progressbar" style="width: 70%;" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>
                                        </div>
                                    </div><span class="h5 mb-0 ml-4">70%</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card card-fullheight">
                        <div class="card-body">
                            <div class="d-flex justify-content-between mb-5">
                                <div>
                                    <h5 class="box-title mb-2">Sales</h5>
                                    <div class="text-muted font-13">Simple Subtitle</div>
                                </div><a class="text-muted" href="#"><i class="ti-info-alt"></i></a>
                            </div>
                            <div><div class="chartjs-size-monitor" style="position: absolute; left: 0px; top: 0px; right: 0px; bottom: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"><div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div></div><div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:200%;height:200%;left:0; top:0"></div></div></div><canvas id="line_chart_1" style="height: 340px; display: block; width: 270px;" width="810" height="1020" class="chartjs-render-monitor"></canvas></div>
                            <div class="d-flex mb-4 mb-sm-0 mt-5">
                                <div class="pr-4 pr-sm-5">
                                    <h6 class="mb-2 font-15 text-muted font-weight-normal">This Week</h6>
                                    <div class="h4 mb-0 text-primary">$32.5k</div>
                                </div>
                                <div class="pl-0 pl-sm-5">
                                    <h6 class="mb-2 font-15 text-muted font-weight-normal">This Month</h6>
                                    <div class="h4 mb-0 text-info">$384.4k</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>    
        </div>    
    </div>
@endsection
@section('extraCss')
    <link href="{{ URL::asset('assets/vendors/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet" />
    <link href="{{ URL::asset('assets/vendors/jvectormap/jquery-jvectormap-2.0.3.css') }}" rel="stylesheet" /> 
    <link href="{{ URL::asset('assets/vendors/DataTables/datatables.min.css') }}" rel="stylesheet" /> 
    <style>
        .data-widget-icon {
            position: absolute;
            top: 20px;
            right: 20px;
            font-size: 40px;
            color: #333;
        } 
    </style>
@endsection
@section('extraJs')
    <script src="{{ URL::asset('assets/vendors/moment/min/moment.min.js') }}"></script>
    <script src="{{ URL::asset('assets/vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ URL::asset('assets/vendors/chart.js/dist/Chart.min.js') }}"></script> 
    <script src="{{ URL::asset('assets/vendors/jvectormap/jquery-jvectormap-2.0.3.min.js') }}"></script>
    <script src="{{ URL::asset('assets/vendors/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script> 
    <script src="{{ URL::asset('assets/vendors/DataTables/datatables.min.js') }}"></script>
    <script src="{{ URL::asset('assets/vendors/easy-pie-chart/dist/jquery.easypiechart.min.js') }}"></script>
    <script type="text/javascript"> 
        $(function() {
            var MONTHS_SH = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
            var DAYS_S = ["S", "M", "T", "W", "T", "F", "S"];
            var DAYS = ["Sunday", "Munday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
            var color = Chart.helpers.color;
            (function() {
                var dr = $('#subheader_daterange');
                if (dr.length) {
                    var t = moment();
                    var a = moment();
                    dr.daterangepicker({
                            startDate: t,
                            endDate: a,
                            ranges: {
                                'Today': [moment(), moment()],
                                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                                'This Month': [moment().startOf('month'), moment().endOf('month')],
                                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                            },
                        }, f),
                        f(t, a, "");
                }

                function f(t, a, r) {
                    var o = "",
                        n = "";
                    a - t < 100 || "Today" == r ?
                        (o = "Today:", n = t.format("MMM D")) :
                        "Yesterday" == r ? (o = "Yesterday:", n = t.format("MMM D")) :
                        n = t.format("MMM D") + " - " + a.format("MMM D"), dr.find(".subheader-daterange-date").html(n), dr.find(".subheader-daterange-title").html(o)
                }
            })();
            $('.easypie').each(function() {
                $(this).easyPieChart({
                    trackColor: $(this).attr('data-trackColor') || '#f2f2f2',
                    scaleColor: false,
                });
            });
            if ($('#line_chart_1').length) {
                var ctx = document.getElementById("line_chart_1").getContext("2d");
                new Chart(ctx, {
                    type: 'line',
                    showScale: false,
                    data: {
                        labels: MONTHS_SH,
                        datasets: [{
                            label: 'Dataset 1',
                            data: [60, 30, 80, 45, 90, 62, 85, 35, 75, 45, 90, 35],
                            data: [10, 30, 20, 40, 30, 50, 40, 60, 50, 70, 60, 80],
                            backgroundColor: color(theme_color('primary')).alpha(0.4).rgbString(),
                            borderColor: theme_color('primary'),
                            fill: false,
                            pointRadius: 5,
                            pointHitRadius: 30,
                            pointHoverBorderWidth: 2,
                            pointHoverRadius: 7,
                            pointBackgroundColor: '#fff',
                        }]
                    },
                    options: {
                        responsive: true,
                        maintainAspectRatio: false,
                        scales: {
                            xAxes: [{
                                gridLines: {
                                    display: false,
                                    max: 60,
                                },
                            }],
                            yAxes: [{
                                gridLines: {
                                    drawBorder: false,
                                },
                                ticks: {
                                    beginAtZero: true,
                                }
                            }]
                        },
                        legend: {
                            labels: {
                                boxWidth: 12
                            }
                        },
                    }
                });
            }
            if ($('#horizonal_bars').length) {
                var ctx = document.getElementById("horizonal_bars").getContext("2d");
                new Chart(ctx, {
                    type: 'horizontalBar',
                    data: {
                        labels: ['PayPal', 'Visa', 'Mastercard', 'Eps', 'JCB', 'Others'],
                        datasets: [{
                            label: 'Dataset 1',
                            backgroundColor: theme_color('info'),
                            //borderColor: theme_color('info'),
                            //borderWidth: 1,
                            data: [80, 70, 60, 50, 40, 30, 20],
                        }]
                    },
                    options: {
                        showScale: false,
                        elementxs: {
                            rectangle: {
                                borderWidth: 2,
                            }
                        },
                        scales: {
                            xAxes: [{
                                gridLine: {
                                    display: false,
                                },
                            }],
                            yAxes: [{
                                gridLines: {
                                    display: false,
                                    drawBorderx: false,
                                },
                                barPercentage: 0.7,
                            }]
                        },
                        responsive: true,
                    }
                });
            }
            if ($('#pie_chart_1').length) {
                var ctx = document.getElementById("pie_chart_1").getContext("2d");
                new Chart(ctx, {
                    type: 'doughnut',
                    data: {
                        labels: ['In-Store Sales', 'Online Sales'],
                        datasets: [{
                            data: [80, 20],
                            backgroundColor: [
                                theme_color('primary'),
                                theme_color('info'),
                            ],
                        }]
                    },
                    options: {
                        responsive: true,
                        legend: {
                            display: false
                        },
                        cutoutPercentage: 65,
                    }
                });
            }
            if ($('#bar_chart_sm').length) {
                var ctx = document.getElementById("bar_chart_sm").getContext("2d");
                new Chart(ctx, {
                    type: 'bar',
                    data: {
                        labels: DAYS,
                        datasets: [{
                            backgroundColor: theme_color("primary"),
                            data: [45, 80, 58, 74, 54, 59, 40]
                        }, {
                            backgroundColor: theme_color('light'),
                            data: [29, 48, 40, 19, 78, 31, 85]
                        }]
                    },
                    options: {
                        title: {
                            display: !1
                        },
                        legend: {
                            display: !1
                        },
                        responsive: !0,
                        maintainAspectRatio: !1,
                        scales: {
                            xAxes: [{
                                display: !1,
                                gridLines: !1,
                            }],
                            yAxes: [{
                                display: !1,
                                gridLines: !1
                            }]
                        },
                        layout: {
                            padding: 0
                        }
                    }
                });
            }
            if ($('#world_map').length) {
                var markers = [{
                        latLng: [55.524010, 105.318756],
                        name: 'Russia',
                        visits: 1000
                    },
                    {
                        latLng: [60.128161, 18.643501],
                        name: 'Sweden',
                        visits: 1000
                    },
                    {
                        latLng: [35.861660, 104.195397],
                        name: 'China',
                        visits: 1000
                    },
                    {
                        latLng: [37.090240, -95.712891],
                        name: 'USA(Neda Shine)',
                        visits: 1000
                    },
                    {
                        latLng: [52.130366, -92.346771],
                        name: 'Canada',
                        visits: 1000
                    },
                    {
                        latLng: [-25.274398, 133.775136],
                        name: 'Austrlia(Neda Shine)',
                        visits: 1000
                    },
                    {
                        latLng: [51.165691, 10.451526],
                        name: 'Germany',
                        visits: 1000
                    },
                    {
                        latLng: [26.02, 50.55],
                        name: 'Bahrain',
                        visits: 1000
                    },
                    {
                        latLng: [-3, -61.38],
                        name: 'Brazil',
                        visits: 1000
                    },
                ];
                $('#world_map').vectorMap({
                    map: 'world_mill_en',
                    backgroundColor: 'transparent',
                    scale: 5,
                    focusOn: {
                        scale: 1,
                        x: 0.5,
                        y: 0.5,
                    },
                    regionStyle: {
                        initial: {
                            fill: '#DADDE0',
                        }
                    },
                    markers: markers,
                    markerStyle: {
                        initial: {
                            fill: theme_color('primary'), // '#ff4081',
                            stroke: '#b9d0ff', // '#ffc6d9',
                            "stroke-width": 5,
                            r: 8
                        },
                        hover: {
                            fill: theme_color('primary'),
                            stroke: '#b9d0ff',
                        }
                    },
                    onMarkerTipShow: function(e, label, index) {
                        label.html('' + markers[index].name + ' (Visits - ' + markers[index].visits);
                    },
                });
            }
            var table = $('#dt-filter').DataTable({
                responsive: true,
                "sDom": 'rtip',
                columnDefs: [{
                    targets: 'no-sort',
                    orderable: false
                }]
            });
            $('#key-search').on('keyup', function() {
                table.search(this.value).draw();
            });
            $('#type-filter').on('change', function() {
                table.column(4).search($(this).val()).draw();
            });
        });
    </script>
@endsection