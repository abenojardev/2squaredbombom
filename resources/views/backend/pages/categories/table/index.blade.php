@php $active = 'configurations' @endphp
@php $subactive = 'categories' @endphp
@extends('backend.layouts.main.index')
@section('title','Categories')
@section('content')
    <div class="page-content fade-in-up"> 
        <div class="page-heading">
            <div class="page-breadcrumb">
                <h1 class="page-title">Product Categories 
                    @if(!is_null($parent)) 
                        <small>
                            <
                            @if(is_null($parent))
                                <a href="{{ URL::route('app.products.categories') }}">
                            @else
                                <a href="{{ URL::route('app.products.categories',$parent->parent_id) }}">
                            @endif
                                {{ $parent->title}}
                            </a>
                        </small>
                    @endif
                </h1> 
            </div> 
            <div>
                @if(!is_null($parent))
                    <a class="btn btn-success" href="{{ URL::route('app.products.filters',$parent->id) }}"> 
                        Filters  
                    </a>
                @endif

                <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#create"> 
                    <span class="ft-plus"></span> Create 
                        @if(!is_null($parent)) Subcategory @endif
                </button> 
            </div> 
        </div>
        <div>
            <div class="card">
                <div class="card-body">
                    <h5 class="box-title"></h5>
                    <div class="table-responsive">
                        <table class="table table-bordered w-100" id="dt-base">
                            <thead class="thead-light">
                                <tr>
                                    <th width="5%"></th>
                                    <th>Category</th>
                                    <th>Sub Categories</th> 
                                    <th>Created At</th>
                                    <th>Updated At</th>
                                    <th>Status</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody> 
                                @foreach($data as $x)
                                    <tr>
                                        <td><img src="{{ URL::route('app.resources').'?src='.$x->photo }}"></td>
                                        <td>
                                            <a href="{{ URL::route('app.products.categories',$x->id) }}">{{ $x->title }}</a>
                                        </td>
                                        <td>{{ $x->sub }}</td> 
                                        <td>{{ $x->created_at }}</td>
                                        <td>{{ $x->updated_at }}</td>
                                        <td>Active</td>
                                        <td>
                                            <div class="mb-4"> 
                                                <div class="btn-group" role="group">
                                                    <button class="btn btn-light dropdown-toggle" id="btnGroupDrop1" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Actions</button>
                                                    <div class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                                        <a class="dropdown-item" data-toggle="modal" data-target="#update_{{ $x->id }}">Update</a>
                                                        <a class="dropdown-item" href="{{ URL::route('app.products.categories.delete',$x->id) }}">Delete</a>
                                                    </div>
                                                </div> 
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('backend.pages.categories.modals.create.index',[ 'parent' => $parent ])
    @include('backend.pages.categories.modals.update.index',[ 'parent' => $data ])
@endsection
@section('extraCss')
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('assets/vendors/DataTables/datatables.min.css') }}">
@endsection
@section('extraJs') 
    <script type="text/javascript" src="{{ URL::asset('assets/vendors/DataTables/datatables.min.js') }}"></script> 
    <script src="{{ URL::asset('assets/js/dt.js') }}"></script>   
@endsection