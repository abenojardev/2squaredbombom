@extends('backend.layouts.authentication.index')
@section('title','Sign In')
@section('content')
    <div class="page-wrapper">
        <div class="auth-wrapper">
            <div class="row auth-content mx-0">
                <div class="col-md-6 bg-white p-5"> 
                    <div class="text-center mb-5">
                        <h4 class="mb-3">Login to Your accaunt</h4>
                        <div class="text-muted">
                            to start using <b>{{ env('APP_NAME') }}</b> 
                        </div>
                    </div>
                    <form id="login-form" method="post" action="{{ URL::route('app.verify') }}">
                        @csrf 

                        @if(session('error'))
                            <p class="error">{{ session('error') }}</p>
                        @endif
                        
                        <div class="form-group mb-4">
                            <input class="form-control" type="text" name="email" placeholder="Email">
                        </div> 
                        <div class="form-group mb-4">
                            <input class="form-control" type="password" name="password" placeholder="Enter Password">
                        </div>
                        <div class="flexbox my-5">
                            <label class="checkbox checkbox-primary">
                                <input type="checkbox" checked="">
                                <span>Remember Me</span>
                            </label> 
                        </div>
                        <div class="text-center"> 
                            <button class="btn btn-primary btn-rounded login-btn">LOGIN</button> 
                        </div>
                    </form> 
                </div>
                <div class="col-md-6 bg-danger text-white p-5 d-flex flex-column justify-content-between">
                    <h3 class="mb-0">{{ env('APP_NAME') }}</h3>
                    <div>
                        <h4 class="font-40 mb-4">Automate your quotations</h4>
                        <p class="mb-0">
                            Lorem ipsum dolor sit amet, consectetur adipisicing
                        </p>
                    </div>
                    <div class="flexbox font-13 text-white-50">
                        <span>2020 © All rights reserved</span>
                    </div>
                </div>
            </div>
        </div> 
    </div> 

    @include('backend.components.loading.index')
@endsection