<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> 
    <title>@yield('title') | {{ env('APP_NAME') }}</title> 
    <link rel="stylesheet" href="{{ URL::asset('assets/fonts/font.google.css') }}" media="all"> 
    <link href="{{ URL::asset('assets/vendors/feather-icons/feather.css') }}" rel="stylesheet" />
    <link href="{{ URL::asset('assets/vendors/@fortawesome/fontawesome-free/css/all.min.css') }}" rel="stylesheet" />
    <link href="{{ URL::asset('assets/vendors/themify-icons/themify-icons.css') }}" rel="stylesheet" />
    <link href="{{ URL::asset('assets/vendors/line-awesome/css/line-awesome.min.css') }}" rel="stylesheet" />
    <link href="{{ URL::asset('assets/vendors/perfect-scrollbar/css/perfect-scrollbar.css') }}" rel="stylesheet" />
    <link href="{{ URL::asset('assets/css/app.min.css') }}" rel="stylesheet" />     
    <link href="{{ URL::asset('assets/css/custom.css') }}" rel="stylesheet" />     
    @yield('extraCss')
</head> 
<body>
    <div class="page-wrapper">
        <div class="content-wrapper">
            @include('backend.components.sidebar.index')
            <div class="content-area"> 
                @include('backend.components.nav.index')
                @yield('content')
                @include('backend.components.footer.index')
            </div>
        </div>
    </div>

    @include('backend.components.loading.index')
    <script src="{{ URL::asset('assets/vendors/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ URL::asset('assets/vendors/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ URL::asset('assets/vendors/metismenu/dist/metisMenu.min.js') }}"></script>
    <script src="{{ URL::asset('assets/vendors/perfect-scrollbar/dist/perfect-scrollbar.min.js') }}"></script> 
    @yield('extraJs')
    <script src="{{ URL::asset('assets/js/app.min.js') }}"></script> 
</body> 
</html>