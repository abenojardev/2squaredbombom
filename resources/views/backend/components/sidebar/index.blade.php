@php $menu = Menu::setup(); @endphp
<div class="page-sidebar custom-scroll" id="sidebar">
    <div class="sidebar-header">
        <a class="sidebar-brand" href="index.html">{{ env('APP_NAME') }}</a>
        <a class="sidebar-brand-mini" href="index.html">P</a>
        <span class="sidebar-points">
            <span class="badge badge-success badge-point mr-2"></span>
            <span class="badge badge-danger badge-point mr-2"></span>
            <span class="badge badge-warning badge-point"></span>
        </span>
    </div>
    <ul class="sidebar-menu metismenu">
        <li class="heading">
            <span>Menu</span>
        </li>
        @foreach($menu as $i => $m)  
            @if(in_array(Auth::user()->type,$m['access'])) 
                <li class="@if($active == $m['active']) mm-active @endif">
                    <a @if($m['type'] == 'multi') 
                            href="javascript:;" 
                        @else 
                             href="{{ URL::route($m['url']) }}"
                        @endif>
                        <i class="sidebar-item-icon {{ $m['icon'] }}"></i>
                        <span class="nav-label">{{ $m['name'] }}</span>
                        @if($m['type'] == 'multi') 
                            <i class="arrow la la-angle-right"></i>
                        @endif
                    </a>
                    @if($m['type'] == 'multi')
                        <ul class="nav-2-level"> 
                            @foreach($m['submenu'] as $is => $ms)
                            <li>
                                <a class="@if(isset($subactive) && $subactive == $ms['active']) mm-active @endif" href="{{ URL::route($ms['url']) }}">
                                    {{ $ms['name'] }}
                                </a>
                            </li> 
                            @endforeach
                        </ul>
                    @endif
                </li>  
            @endif 
        @endforeach
    </ul>
</div>