<footer class="page-footer flexbox">
    <div class="text-muted">
    	2020 © <strong>{{ env('APP_NAME') }}</strong>. All rights reserved
    </div> 
</footer>