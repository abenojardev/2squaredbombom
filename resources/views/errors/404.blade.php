@extends('frontend.components.layouts.main.index')
@section('title','Page not found')
@section('content')  
	<section class="ratio_45">
	    <div class="container">
	         <div class="row">
	            <div class="col">
	                <div class="title4">
	                    <h1 class="title-inner4">404</h1> 
	                </div>
	            </div>
	        </div>
	        <div class="row">
	        	<div class="col">
	        		<center> 
		        		<h2>Page not found</h2>
	        		</center>
	        	</div>
	        </div>
	    </div>
	</section> 
	<br><br>
	@include('frontend.components.ads.landscape.index')
	<br><br>
@endsection