<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuotationItems extends Model
{
    protected $table = 'quotation_items';

    protected $fillable = [
 		'quotation_id','product_id','parent_id','price','qty','status','variations'
    ];

    protected $casts = [
    	'variations'
    ];
}
