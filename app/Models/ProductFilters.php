<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductFilters extends Model
{
	protected $table = 'product_filters';

	protected $fillable = ['title','category_id','status'];
}
