<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Quotation extends Model
{
    protected $table = 'quotation';

    protected $fillable = [
    	'user_id','members','company_name', 'country','date_needed','estimatedordersched','parent_id','quotation_status','status'
    ];
}
