<?php

namespace App\Http\Middleware\Acl;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class SuperNotAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    { 
        if (Auth::check()){   
            return redirect()->route('app.dashboard'); 
        }

        return $next($request);
    }
}
