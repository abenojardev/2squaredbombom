<?php

namespace App\Http\Controllers\API\Product\Category;

use App\Http\Controllers\Controller; 
use App\Models\ProductCategories;
use App\Repositories\CategoryRepository;
use App\Models\Product;
use App\Repositories\ProductRepository;
use App\Models\ProductFilters;
use App\Repositories\FilterRepository;
use Illuminate\Http\Request; 

class ViewController extends Controller
{  
    protected $categories,$request,$product,$filters;

    public function __construct(ProductCategories $categories,ProductFilters $filters,Product $product,Request $request)
    {
        $this->request = $request;
        $this->categories = new CategoryRepository($categories);
        $this->product = new ProductRepository($product);
        $this->filters = new FilterRepository($filters);
    }

    public function index()
    {      
        return response()->json([
            'status' => 200,
            'data' => $this->data()
        ],200,[],JSON_PRETTY_PRINT);
    }

    public function data()
    {         
        return $this->categories->getCategoriesForTables($this->request->id); 
    }

    public function subCategories($slug)
    {
        return [
            'parent' => $this->categories->getParentBySlug($slug),
            'child' => $this->categories->getCategoryBySlug($slug)
        ];
    }

    public function getproducts($params)
    {
        return $this->product->getProductsByManyId($params);
    }

    public function getproduct($name)
    {
        $product = $this->product->getProductBySlug($name);

        return $product;
    }

    public function products($slug,$params)
    { 
        $category = $this->categories->getParentBySlug($slug);
        $products = $this->product->getMainProductsByCategory($category->id,$params);
        $filters  = $this->filters->getFiltersForAll($category->id);

        return [ 
            'products' => $products,
            'filters' => $filters 
        ];
    }
}
