<?php

namespace App\Http\Controllers\Frontend\Pages;

use App\Http\Controllers\Controller;  
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\QuotationItems;
use App\Models\Quotation;
use Storage,URL,App,Redirect,Auth,Hash; 

class HomepageController extends Controller
{  
    protected $categories,$request,$filters,$products;

    public function __construct(Request $request)
    {
        $this->request = $request; 
    }
  
    public function index()
    {  
        $data = App::call('App\Http\Controllers\API\Product\Category\ViewController@data');
 
        return view('frontend.pages.homepage.index')
            ->with([
                'title' => 'Welcome to '.getenv('APP_NAME'),
                'categories' => $data
            ]);
    }  

    public function categoriesview($category)
    {
        $params = $this->request->except('_token');
        $data = App::call('App\Http\Controllers\API\Product\Category\ViewController@subCategories',[ 'slug' => $category]);
        $products = App::call('App\Http\Controllers\API\Product\Category\ViewController@products',[ 'slug' => $category, 'params' => $params ]);
 
        return view('frontend.pages.categories.index')
            ->with([
                'title' => $data['parent']->title,
                'categories' => $data,
                'bread' => [
                    'title' => (count($data['child']) <= 0) ? 'Products' : 'Sub Categories',
                    'links' => [
                        [ 
                            'url' => URL::route('client.homepage'),
                            'title' => 'Home'
                        ],
                        [ 
                            'url' => '#',
                            'title' => (count($data['child']) <= 0) ? 'Products' : 'Categories'
                        ],
                        [ 
                            'url' => '#',
                            'title' => $data['parent']->title
                        ]
                    ]
                ],
                'products' => $products
            ]);
    }

    public function compare()
    {
        $params = $this->request->session()->get('compare.items'); 
        $products = App::call('App\Http\Controllers\API\Product\Category\ViewController@getproducts',[ 'params' => $params ]);
 
        return view('frontend.pages.compare.index')
            ->with([
                'title' => 'Compare Products', 
                'bread' => [
                    'title' => 'Compare Products',
                    'links' => [
                        [ 
                            'url' => URL::route('client.homepage'),
                            'title' => 'Home'
                        ], 
                        [ 
                            'url' => '#',
                            'title' => 'Compare'
                        ]
                    ]
                ],
                'products' => $products
            ]);

    }

    public function addtocompare($id)
    {   
        if(!$this->request->session()->has('compare.items')) {
            $items = $this->request->session()->put('compare.items',[]);
        }

        if (!in_array($id, $this->request->session()->get('compare.items'))) {  
            $this->request->session()->push('compare.items',$id);
        }

        return back();
    }

    public function login()
    {
        if(Auth::user()){
            return Redirect::route('client.homepage');
        }

        return view('frontend.pages.login.index')
            ->with([
                'title' => 'Login', 
                'bread' => [
                    'title' => 'Login',
                    'links' => [
                        [ 
                            'url' => URL::route('client.homepage'),
                            'title' => 'Home'
                        ],  
                        [ 
                            'url' => '#',
                            'title' => 'Login Account'
                        ]
                    ]
                ]
            ]);
    }

    public function quotations()
    {  
        return view('frontend.pages.quotations.index')
            ->with([
                'title' => 'Quotations', 
                'bread' => [
                    'title' => 'Quotations',
                    'links' => [
                        [ 
                            'url' => URL::route('client.homepage'),
                            'title' => 'Home'
                        ],  
                        [ 
                            'url' => '#',
                            'title' => 'Quotations'
                        ]
                    ]
                ],
                'data' => Quotation::whereUser_id(Auth::user()->id)->get()
            ]);
    }

    public function savecheckout()
    {
        $id = Quotation::insertGetId([
            'user_id' => Auth::user()->id,
            'members' => json_encode([1,3,4]),
            'company_name' => $this->request->company_name,
            'country' => $this->request->country,
            'date_needed' => $this->request->date_needed,
            'estimatedordersched' => $this->request->estimatedordersched,
            'parent_id' => 0,
            'quotation_status' => 'pending',
            'status' => 1
        ]);

        $params = $this->request->session()->get('cart.items');  

        foreach ($params as $k => $y) {
            QuotationItems::insert([
                'quotation_id' => $id,
                'product_id' => $y['product_id'],
                'parent_id' => $y['parent_id'],
                'qty' => $y['qty'],
                'variations' => json_encode($y['variations']),
                'status' => 1
            ]);
        }
        $this->request->session()->forget('cart.items');
        return Redirect::route('client.quotations');
    }

    public function register()
    {
        $data = [
            'name' => $this->request->name,
            'email' => $this->request->email,
            'password' => Hash::make($this->request->password),
            'type' => 'user',
            'status' => 1
        ];

        User::create($data);

        return back()->withSuccess('Successfully registered, you may now login !');
    }

    public function logout()
    {
        Auth::logout();

        return Redirect::route('client.homepage');
    }
    public function verify()
    {
        $data = [ 
            'email' => $this->request->email,
            'password' => $this->request->password,
            'type' => 'user',
            'status' => 1
        ];

        if(Auth::attempt($data)){
            return Redirect::route('client.cart');
        }

        return back()->withError('Invalid login credentials !');
    }

    public function checkout()
    { 
        if(!Auth::user()) {
            return Redirect::route('client.login');
        } elseif(Auth::user()->type != 'user') {
            return Redirect::route('client.login');
        }

        return view('frontend.pages.checkout.index')
            ->with([
                'title' => 'Checkout Cart', 
                'bread' => [
                    'title' => 'Checkout Cart',
                    'links' => [
                        [ 
                            'url' => URL::route('client.homepage'),
                            'title' => 'Home'
                        ], 
                        [ 
                            'url' => URL::route('client.cart'),
                            'title' => 'View Cart'
                        ], 
                        [ 
                            'url' => '#',
                            'title' => 'Checkout Cart'
                        ]
                    ]
                ]
            ]);
    }

    
    public function quotationsview($id)
    { 
        $items = QuotationItems::whereQuotation_id($id)->get();
        
        return view('frontend.pages.quotations.details')
            ->with([
                'title' => 'Quotation Details', 
                'bread' => [
                    'title' => 'Quotation Details',
                    'links' => [
                        [ 
                            'url' => URL::route('client.homepage'),
                            'title' => 'Home'
                        ], 
                        [ 
                            'url' => URL::route('client.quotations'),
                            'title' => 'My Quotations Request'
                        ], 
                        [ 
                            'url' => '#',
                            'title' => 'Quotation Details'
                        ]
                    ]
                ],
                'items' => $items
            ]);
    }

    public function cart()
    { 
        $params = $this->request->session()->get('cart.items');  
 
        return view('frontend.pages.cart.index')
            ->with([
                'title' => 'View Cart', 
                'bread' => [
                    'title' => 'View Cart',
                    'links' => [
                        [ 
                            'url' => URL::route('client.homepage'),
                            'title' => 'Home'
                        ], 
                        [ 
                            'url' => '#',
                            'title' => 'View Cart'
                        ]
                    ]
                ],
                'items' => $params
            ]);
    }

    public function addtocart()
    {   
        $data = $this->request->except('_token');

        foreach ($data['variations'] as $k => $v) {
            $data['variations'][$k] = json_decode($v,true);
        }

        if(!$this->request->session()->has('cart.items')) {
            $items = $this->request->session()->put('cart.items',[]);
        }
 
        $this->request->session()->push('cart.items',$data); 

        return back();
    }

    public function removeItemsFromCompare()
    {   
        $this->request->session()->forget('compare.items');

        return back();
    }

    public function clearcart()
    {   
        $this->request->session()->forget('cart.items');

        return back();
    }

    public function productview($name)
    {
        $data = App::call('App\Http\Controllers\API\Product\Category\ViewController@getproduct',[ 'name' => $name ]);
  
        return view('frontend.pages.products.details.index')
            ->with([
                'title' => $data->name, 
                'bread' => [
                    'title' => $data->name,
                    'links' => [
                        [ 
                            'url' => URL::route('client.homepage'),
                            'title' => 'Home'
                        ], 
                        [ 
                            'url' => URL::route('client.categories',str_replace(' ','-',strtolower(App\Models\ProductCategories::find($data->category_id)->title))),
                            'title' => App\Models\ProductCategories::find($data->category_id)->title
                        ]
                    ]
                ],
                'data' => $data
            ]);
 
    }
}
