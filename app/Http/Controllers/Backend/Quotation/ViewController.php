<?php

namespace App\Http\Controllers\Backend\Quotation;

use App\Http\Controllers\Controller; 
use App\Models\Product;
use App\Repositories\ProductRepository;
use App\Models\Quotation; 
use App\Models\QuotationItems; 
use App\Repositories\QuotationRepository;
use App\Repositories\QuotationItemRepository;
use Illuminate\Http\Request;
use Storage;

class ViewController extends Controller
{  
    protected $quotation,$request,$items,$products;

    public function __construct(Product $products,Quotation $quotation,QuotationItems $items,Request $request)
    {
        $this->request = $request;
        $this->products = new ProductRepository($products);
        $this->quotation = new QuotationRepository($quotation);
        $this->items = new QuotationItemRepository($items);
    }
  
    public function index()
    {  
        $data = $this->quotation->getAllQuotations();

        return view('backend.pages.quotation.table.index')
            ->with([
                'data' => $data
            ]);
    }

    public function view($id)
    {  
        $data = $this->quotation->getById($id);
        $products = $this->items->getItemsByParent($id);

        return view('backend.pages.quotation.view.index')
            ->with([
                'data' => $data,
                'products' => $products
            ]);
    }

}
