<?php

namespace App\Http\Controllers\Backend\Quotation;

use App\Http\Controllers\Controller; 
use App\Models\Product;
use App\Repositories\ProductRepository;
use App\Models\Quotation; 
use App\Models\QuotationItems; 
use App\Repositories\QuotationRepository;
use App\Repositories\QuotationItemRepository;
use Illuminate\Http\Request;
use Storage;

class UpdateController extends Controller
{  
    protected $quotation,$request,$items,$products;

    public function __construct(Product $products,Quotation $quotation,QuotationItems $items,Request $request)
    {
        $this->request = $request;
        $this->products = new ProductRepository($products);
        $this->quotation = new QuotationRepository($quotation);
        $this->items = new QuotationItemRepository($items);
    }
  
    public function index()
    {  
        $attr = $this->request->except('_token');
        $errors = false;
        
        foreach ($attr as $k => $v) {
            $save = $this->items->update($k,[
                'price' => $v
            ]);

            if (!$save) {
                $errors = true;
            }
        }

        return $errors ? back()->withError('Something went wrong !') : back()->withSuccess('Quotation has been saved !');
    }
 
}
