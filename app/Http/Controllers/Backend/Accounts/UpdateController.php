<?php

namespace App\Http\Controllers\Backend\Accounts;

use App\Http\Controllers\Controller; 
use App\Models\User;
use App\Repositories\UserRepository; 
use Illuminate\Http\Request;
use Storage;

class UpdateController extends Controller
{  
    protected $request,$users,$id;

    public function __construct(User $users,Request $request)
    {
        $this->request = $request;
        $this->users = new UserRepository($users);  
    }
  
    public function enable($id)
    {
        $this->id = $id;

        $attr = [
            'status' => 1
        ];

        return $this->save($attr);
    }

    public function disable($id)
    { 
        $this->id = $id;

        $attr = [
            'status' => 0
        ];

        return $this->save($attr);
    }

    public function save($attr)
    { 

        $save = $this->users->update($this->id,$attr);

        return back();
    }
}