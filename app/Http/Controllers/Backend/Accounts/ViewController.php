<?php

namespace App\Http\Controllers\Backend\Accounts;

use App\Http\Controllers\Controller; 
use App\Models\User;
use App\Repositories\UserRepository; 
use Illuminate\Http\Request;
use Storage;

class ViewController extends Controller
{  
    protected $request,$users;

    public function __construct(User $users,Request $request)
    {
        $this->request = $request;
        $this->users = new UserRepository($users); 
    }
  
    public function index()
    {
        $data = $this->users->getUsersByType('user'); 

        return view('backend.pages.accounts.table.index')
            ->with([
                'data' => $data, 
                'type' => 'users'
            ]);
    }

    public function suppliers()
    {
        $data = $this->users->getUsersByType('supplier'); 

        return view('backend.pages.accounts.table.index')
            ->with([
                'data' => $data, 
                'type' => 'suppliers'
            ]);
    }

    public function manufacturers()
    { 
        $data = $this->users->getUsersByType('manufacturer'); 

        return view('backend.pages.accounts.table.index')
            ->with([
                'data' => $data, 
                'type' => 'manufacturers'
            ]);
    }
}