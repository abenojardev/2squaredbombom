<?php

namespace App\Http\Controllers\Backend\Product\Categories;

use App\Http\Controllers\Controller; 
use App\Models\ProductCategories;
use App\Repositories\CategoryRepository;
use Illuminate\Http\Request;
use Storage;

class ViewController extends Controller
{  
    protected $categories,$request;

    public function __construct(ProductCategories $categories,Request $request)
    {
        $this->request = $request;
        $this->categories = new CategoryRepository($categories);
    }

    public function index($id = null)
    {     
        $data = $this->categories->getCategoriesForTables($id);
        $parent = $this->categories->getParentCategory($id);

        return view('backend.pages.categories.table.index')
            ->with([
                'data' => $data,
                'parent' => $parent
            ]);
    }  
}
