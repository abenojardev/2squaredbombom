<?php

namespace App\Http\Controllers\Backend\Product\Categories;

use App\Http\Controllers\Controller; 
use App\Models\ProductCategories;
use App\Repositories\CategoryRepository;
use Illuminate\Http\Request;
use App\Services\DiskStorage;

class DeleteController extends Controller
{  
    protected $categories,$request;

    public function __construct(ProductCategories $categories,Request $request)
    {
        $this->request = $request;
        $this->categories = new CategoryRepository($categories);
    }

    public function index($id)
    {  
        $save = $this->categories->delete($id);
        
        return $save ? back() : abort(500);
    } 
}
