<?php

namespace App\Http\Controllers\Backend\Product\Categories;

use App\Http\Controllers\Controller; 
use App\Models\ProductCategories;
use App\Repositories\CategoryRepository;
use Illuminate\Http\Request;
use App\Services\DiskStorage;

class CreateController extends Controller
{  
    protected $categories,$request;

    public function __construct(ProductCategories $categories,Request $request)
    {
        $this->request = $request;
        $this->categories = new CategoryRepository($categories);
    }

    public function index()
    {
 
        $attributes = [
            'parent_id' => $this->request->parent_id,
            'title' => $this->request->title,
            'photo' => $this->upload(),
            'status' => 1
        ];

        $save = $this->categories->create($attributes);
        
        return $save ? back() : abort(500);
    }

    public function upload()
    {
        $disk = new DiskStorage($this->request->photo,'categories');
        return $disk->generate();
    }
}
