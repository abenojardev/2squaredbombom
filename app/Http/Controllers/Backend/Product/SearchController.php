<?php

namespace App\Http\Controllers\Backend\Product;

use App\Http\Controllers\Controller; 
use App\Models\Product;
use App\Repositories\ProductRepository;
use App\Models\ProductCategories;
use App\Models\ProductFilters;
use App\Repositories\CategoryRepository;
use App\Repositories\FilterRepository;
use Illuminate\Http\Request;
use Storage;

class SearchController extends Controller
{  
    protected $categories,$request,$filters,$products;

    public function __construct(Product $products,ProductFilters $filters,ProductCategories $categories,Request $request)
    {
        $this->request = $request;
        $this->products = new ProductRepository($products);
        $this->categories = new CategoryRepository($categories);
        $this->filters = new FilterRepository($filters);
    }
  
    public function index()
    {
        $data = [];
        $q = $this->request->q;
        $categories = $this->categories->getCategoriesForTables(null);

        if ($this->request->has('q')) {
            $data = $this->products->searchProduct($q);
        }

        return view('backend.pages.products.search.index')
            ->with([
                'data' => $data,
                'category' => $categories,
                'q' => $q
            ]);
    }
}