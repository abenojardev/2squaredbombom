<?php

namespace App\Http\Controllers\Backend\Product;

use App\Http\Controllers\Controller; 
use App\Models\Product;
use App\Repositories\ProductRepository;
use Illuminate\Http\Request;
use App\Services\DiskStorage;
use Auth;

class UpdateController extends Controller
{  
    protected $product,$request;

    public function __construct(Product $product,Request $request)
    {
        $this->request = $request;
        $this->product = new ProductRepository($product);
    }

    public function index($id)
    {  
        $attributes = [  
            'name' => $this->request->name,
            'description' => $this->request->description,
            'part_number' => $this->request->partnumber,
            'srp' => $this->request->srp,
            'is_available' => $this->request->availability 
        ];

        if($this->request->has('photo') && isset($this->request->photo)) {
            $attributes['photo'] = $this->upload();
        }

        $save = $this->product->update($id,$attributes);
        
        return $save ? back() : abort(500);
    }

    public function specs($id)
    { 
        $attributes = [  
            'specs' => ($this->request->specs)
        ];
 
        $save = $this->product->update($id,$attributes);

        return $save ? back() : abort(500);
    }

    public function filter($id)
    {  

        $attributes = [ 
            'filters' => ($this->request->except('_token'))
        ];
 
        $save = $this->product->update($id,$attributes);
        
        return $save ? back() : abort(500);
    }

    public function upload()
    {
        $disk = new DiskStorage($this->request->photo,'products');
        return $disk->generate();
    }
}
