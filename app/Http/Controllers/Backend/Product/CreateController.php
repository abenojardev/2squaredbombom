<?php

namespace App\Http\Controllers\Backend\Product;

use App\Http\Controllers\Controller; 
use App\Models\Product;
use App\Repositories\ProductRepository;
use Illuminate\Http\Request;
use App\Services\DiskStorage;
use Auth;

class CreateController extends Controller
{  
    protected $product,$request;

    public function __construct(Product $product,Request $request)
    {
        $this->request = $request;
        $this->product = new ProductRepository($product);
    }

    public function index()
    {  
        $attributes = [
            'user_id' => Auth::user()->id,
            'type' => 'main', 
            'category_id' => $this->request->category_id,
            'name' => $this->request->name,
            'description' => $this->request->description,
            'part_number' => $this->request->partnumber,
            'srp' => $this->request->srp,
            'is_available' => $this->request->availability,
            'photo' => $this->upload(),
            'status' => 1
        ];

        $save = $this->product->create($attributes);
        
        return $save ? back() : abort(500);
    }

    public function addTo($id)
    {

        $attributes = [
            'user_id' => Auth::user()->id,
            'type' => 'inherited', 
            'product_id' => $id, 
            'status' => 1
        ];

        $save = $this->product->create($attributes);
        
        return $save ? back()->withSuccess('This product has been added to your list !') : abort(500);
    }

    public function upload()
    {
        $disk = new DiskStorage($this->request->photo,'products');
        return $disk->generate();
    }
}
