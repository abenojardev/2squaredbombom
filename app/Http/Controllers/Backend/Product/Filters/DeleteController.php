<?php

namespace App\Http\Controllers\Backend\Product\Filters;

use App\Http\Controllers\Controller; 
use App\Models\ProductFilters;
use App\Repositories\FilterRepository;
use Illuminate\Http\Request;
use App\Services\DiskStorage;

class DeleteController extends Controller
{  
    protected $filters,$request;

    public function __construct(ProductFilters $filters,Request $request)
    {
        $this->request = $request;
        $this->filters = new FilterRepository($filters);
    }

    public function index($id)
    {  
        $save = $this->filters->delete($id);
        
        return $save ? back() : abort(500);
    } 
}
