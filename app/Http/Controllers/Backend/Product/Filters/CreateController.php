<?php

namespace App\Http\Controllers\Backend\Product\Filters;

use App\Http\Controllers\Controller; 
use App\Models\ProductFilters;
use App\Repositories\FilterRepository;
use Illuminate\Http\Request;
use App\Services\DiskStorage;

class CreateController extends Controller
{  
    protected $filters,$request;

    public function __construct(ProductFilters $filters,Request $request)
    {
        $this->request = $request;
        $this->filters = new FilterRepository($filters);
    }

    public function index()
    {
 
        $attributes = [
            'category_id' => $this->request->category_id,
            'title' => $this->request->title, 
            'status' => 1
        ];

        $save = $this->filters->create($attributes);
        
        return $save ? back() : abort(500);
    } 
}
