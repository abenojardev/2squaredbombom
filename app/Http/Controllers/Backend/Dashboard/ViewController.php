<?php

namespace App\Http\Controllers\Backend\Dashboard;

use App\Http\Controllers\Controller;  
use Illuminate\Http\Request;
use Storage;

class ViewController extends Controller
{  
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request; 
    }
  
    public function index()
    {  
        return view('backend.pages.dashboard.index');
    } 
}