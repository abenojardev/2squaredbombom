<?php

namespace App\Http\Controllers\Backend\Auth\Backend;

use App\Http\Controllers\Controller;  
use Illuminate\Http\Request; 
use App\Models\User;
use Auth,Redirect;

class VerifyController extends Controller
{   

	protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index()
    {
    	$credentials = [
    		'email' => $this->request->email,
    		'password' => $this->request->password,
    		'status' => 1
    	];

    	if(Auth::attempt($credentials)){

            if(Auth::user()->type == 'user') { 
                Auth::logout();
                return back()->withError('Account not permitted !');
            }

    		return Redirect::route('app.dashboard'); 

    	}

    	return back()->withError('Invalid login credentials !');
    }
}
