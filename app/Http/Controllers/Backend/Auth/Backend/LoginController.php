<?php

namespace App\Http\Controllers\Backend\Auth\Backend;

use App\Http\Controllers\Controller;  

class LoginController extends Controller
{   

    public function __construct()
    {
        //
    }

    public function index()
    {
        return view('backend.pages.authentication.login.backend.index');     
    }
}
