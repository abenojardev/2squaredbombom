<?php 

namespace App\Services; 

/**
 * Returns object of menu for users navigation
 *
 * @class MenuNavigation
 */ 
 
class MenuNavigation {  

    /**
     * constant type types
     *
     * @var type
     */  
    const A = 'admin';
    const B = 'supplier';
    const C = 'manufacturer';

    public static function setup()
    {    
        $type = new MenuNavigation;

        return [
            [
                'type' => 'single',
                'active' => 'dashboard',
                'icon' => 'ft-home',
                'name' => 'Dashboard',
                'url' => 'app.dashboard',
                'access' => [
                    $type::A,
                    $type::B,
                    $type::C
                ]
            ],
            [
                'type' => 'single',
                'active' => 'products',
                'icon' => 'ft-shopping-cart',
                'name' => 'Products to sell',
                'url' => 'app.products.tosell',
                'access' => [
                    $type::B
                ]
            ],
            [
                'type' => 'single',
                'active' => 'search',
                'icon' => 'ft-search',
                'name' => 'Search Products',
                'url' => 'app.products.search',
                'access' => [
                    $type::A,
                    $type::B,
                    $type::C
                ]
            ],
            [
                'type' => 'single',
                'active' => 'products',
                'icon' => 'ft-shopping-cart',
                'name' => 'Products',
                'url' => 'app.products',
                'access' => [
                    $type::A, 
                    $type::C
                ]
            ],
            [
                'type' => 'multi',
                'active' => 'configurations',
                'icon' => 'ft-list',
                'name' => 'Configurations',
                'url' => '',
                'access' => [
                    $type::A,$type::C
                ],
                'submenu' => [
                    [
                        'active' => 'categories',
                        'name' => 'Categories & Filters',
                        'url' => 'app.products.categories'
                    ]
                ]
            ],  
            [
                'type' => 'single',
                'active' => 'quotations',
                'icon' => 'ft-mail',
                'name' => 'Quotations',
                'url' => 'app.quotations',
                'access' => [
                    $type::A, 
                    $type::B, 
                    $type::C
                ]
            ],
            [
                'type' => 'multi',
                'active' => 'accounts',
                'icon' => 'ft-users',
                'name' => 'Accounts',
                'url' => '',
                'access' => [
                    $type::A
                ],
                'submenu' => [
                    [
                        'active' => 'suppliers',
                        'name' => 'Suppliers',
                        'url' => 'app.accounts.suppliers'
                    ],
                    [
                        'active' => 'manufacturers',
                        'name' => 'Manufacturers',
                        'url' => 'app.accounts.manufacturers'
                    ],
                    [
                        'active' => 'users',
                        'name' => 'Users',
                        'url' => 'app.accounts.users'
                    ]
                ]
            ],   
            [
                'type' => 'multi',
                'active' => 'reports',
                'icon' => 'ft-file',
                'name' => 'Reports',
                'url' => '',
                'access' => [
                    $type::A
                ],
                'submenu' => [
                    [
                        'active' => 'suppliers',
                        'name' => 'Accounts',
                        'url' => 'app.dashboard'
                    ],
                    [
                        'active' => 'Products',
                        'name' => 'Products',
                        'url' => 'app.dashboard'
                    ],
                    [
                        'active' => 'Categories',
                        'name' => 'Categories',
                        'url' => 'app.dashboard'
                    ],
                    [
                        'active' => 'Filters',
                        'name' => 'Filters',
                        'url' => 'app.dashboard'
                    ],
                    [
                        'active' => 'quotations',
                        'name' => 'Quotations',
                        'url' => 'app.dashboard'
                    ],
                    [
                        'active' => 'quotations',
                        'name' => 'Invoices',
                        'url' => 'app.dashboard'
                    ]
                ]
            ],  
            [
                'type' => 'single',
                'active' => 'logout',
                'icon' => 'ft-settings',
                'name' => 'Logout',
                'url' => 'app.logout',
                'access' => [
                    $type::A,
                    $type::B,
                    $type::C
                ],
            ]
        ]; 
    }
}