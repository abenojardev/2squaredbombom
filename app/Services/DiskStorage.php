<?php 

namespace App\Services;
use Storage,File;

// image intervention add here for photo manipulation
class DiskStorage
{
	protected $file;
	protected $path;  

	function __construct($file,$path)
	{
		$this->file = $file;
		$this->path = $path; 
	}

	public function generate()
	{
		$save = Storage::disk('local')->putFileAs($this->path,$this->file,$this->name());

 		if ($save) {
 			return $save;
 		}

 		return null;
	}

	public function name()
	{ 
		return rand(00000000,9999999).'_'.strtotime(date('M d, Y')).'.'.$this->extension(); 
	}

	public function extension()
	{
		return $this->file->extension();
	}
}