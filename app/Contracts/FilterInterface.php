<?php 

namespace App\Contracts; 

interface FilterInterface {
	
	public function getFiltersForTables($id);

	public function getFiltersForAll($id);
	
}