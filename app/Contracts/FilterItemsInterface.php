<?php 

namespace App\Contracts; 

interface FilterItemsInterface {

	public function getFiltersItemsForTables($id);
	
}