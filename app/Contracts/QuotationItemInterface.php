<?php 

namespace App\Contracts; 

interface QuotationItemInterface {

	public function getItemsByParent($id);
	
}