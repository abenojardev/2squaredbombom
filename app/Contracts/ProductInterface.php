<?php 

namespace App\Contracts; 

interface ProductInterface {
	
	public function getMainProducts();

	public function getInheritedProducts();

	public function getMainProductsByCategory($id,$params);
	
	public function searchProduct($q);

	public function getProductsByManyId($params);

	public function getProductBySlug($name);
	
}