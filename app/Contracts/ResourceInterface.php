<?php 

namespace App\Contracts; 

interface ResourceInterface {

	public function getById($id);
	
	public function getAll($parameters = null,$optionals = null,$status = 1,$paginate = false,$sql = false); 
  
	public function getCount($parameters = null,$optionals = null,$status = 1,$paginate = false,$sql = false);

	public function create(array $attributes);
  
	public function update($id,array $attributes);

	public function delete($id);

}