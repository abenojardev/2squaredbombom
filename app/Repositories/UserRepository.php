<?php 

namespace App\Repositories;
  
use Illuminate\Database\Eloquent\Model; 
use App\Contracts\UserInterface; 
use App\Repositories\ResourceRepo;

class UserRepository extends ResourceRepo implements UserInterface
{
	
	protected $model; 

	public function __construct(Model $model)
	{
		$this->model = $model; 
	}   

	public function getUsersByType($type)
	{
		return $this->getAll([
			'type' => $type
		],null,'any');
	}
}