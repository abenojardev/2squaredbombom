<?php 

namespace App\Repositories;
  
use Illuminate\Database\Eloquent\Model; 
use App\Contracts\QuotationItemInterface; 
use App\Repositories\ResourceRepo;
use App\Models\Product;
use Auth;

class QuotationItemRepository extends ResourceRepo implements QuotationItemInterface
{
	
	protected $model; 

	public function __construct(Model $model)
	{
		$this->model = $model; 
	}   

	public function getItemsByParent($id)
	{
		$data = [];

		$items = $this->getAll([
			'quotation_id' => $id,
			'status' => 1
		]);

		foreach ($items as $y => $x) {
			$product = Product::find($x->product_id);

			if ($product->user_id != Auth::user()->id) {
				unset($items[$y]);
			} else {
				if($product->type != 'main'){
					$items[$y]->details = Product::find($product->product_id); 
				} else {
					$items[$y]->details = $product;
				}
			}
		} 

		return $items;
	}
}