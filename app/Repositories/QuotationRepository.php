<?php 

namespace App\Repositories;
  
use Illuminate\Database\Eloquent\Model; 
use App\Contracts\QuotationInterface; 
use App\Repositories\ResourceRepo;
use Auth;

class QuotationRepository extends ResourceRepo implements QuotationInterface
{
	
	protected $model; 

	public function __construct(Model $model)
	{
		$this->model = $model; 
	}   

	public function getAllQuotations()
	{
		$data = $this->getAll(
			null,
			[
				[
					'col' => 'members',
					'operand' => 'LIKE',
					'val' => Auth::user()->id
				]
			]
		);

		foreach ($data as $key => $value) { 
			if (!in_array(Auth::user()->id, json_decode($value->members))) { 
				unset($data[$key]);
			}
		}

		return $data;
	}
}