<?php 

namespace App\Repositories;
 
use Illuminate\Database\Eloquent\Model; 
use App\Contracts\ProductInterface; 
use App\Repositories\ResourceRepo;
use Auth;

class ProductRepository extends ResourceRepo implements ProductInterface
{
	
	protected $model; 

	function __construct(Model $model)
	{
		$this->model = $model; 
	}  

	public function getMainProducts()
	{
		return $this->getAll([
			'type' => 'main',
			'user_id' => Auth::user()->id
		]);
	}

	public function getMainProductsByCategory($id,$params)
	{  
		$opts = null;

		if (array_key_exists('q',$params)) {
			$opts = [ 
				[
					'col' => 'name',
					'operand' => 'LIKE',
					'val' => $params['q']
				],
				[
					'col' => 'description',
					'operand' => 'LIKE',
					'val' => $params['q']
				],
				[
					'col' => 'part_number',
					'operand' => 'LIKE',
					'val' => $params['q']
				]
			];
		}

		return $this->getAll([
			'type' => 'main',
			'category_id' => $id
		],$opts);
	}

	public function getInheritedProducts()
	{ 
		$data = [];
		$prods = $this->getAll([
			'type' => 'inherited',
			'user_id' => Auth::user()->id
		]);

		foreach ($prods as $key => $value) {
			array_push($data,$this->getById($value->product_id));
		}

		return $data;
	}

	public function searchProduct($q)
	{
		if(Auth::user()->type == 'supplier') {
			$filter = [
				'type' => 'main'
			];
		} else {
			$filter = [
				'type' => 'main',
				'user_id' => Auth::user()->id
			];
		}

		return $this->getAll(
			$filter,
			[
				[
					'col' => 'name',
					'operand' => 'LIKE',
					'val' => $q
				],
				[
					'col' => 'description',
					'operand' => 'LIKE',
					'val' => $q
				],
				[
					'col' => 'part_number',
					'operand' => 'LIKE',
					'val' => $q
				],
				[
					'col' => 'specs',
					'operand' => 'LIKE',
					'val' => $q
				]
			]
		);
	}

	public function getProductsByManyId($params)
	{
		return $this->model->findMany($params);
	}


	public function getProductBySlug($name) 
	{ 
		$slug = strtolower(str_replace('-', ' ', $name));

		$data = $this->getAll(null,[
			[
				'col' => 'name',
				'operand' => 'LIKE',
				'val' => $slug
			]
		]);
 
		if ($data->count() <= 0) {
			abort(404);
		}

		return $this->getById($data->first()->id);
	}
}