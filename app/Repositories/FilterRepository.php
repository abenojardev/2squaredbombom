<?php 

namespace App\Repositories;
  
use Illuminate\Database\Eloquent\Model; 
use App\Contracts\FilterInterface; 
use App\Repositories\ResourceRepo; 
use App\Models\ProductFiltersItems; 

class FilterRepository extends ResourceRepo implements FilterInterface
{
	
	protected $model; 

	function __construct(Model $model)
	{
		$this->model = $model; 
	} 

	public function getFiltersForTables($id)
	{
		$data = $this->getAll(['category_id'=> $id]);

		foreach ($data as $key => $value) {
			$value->items = ProductFiltersItems::where('product_filters_id',$value->id)->count();
		}

		return $data;
	}

	public function getFiltersForAll($id)
	{
		$data = $this->getAll(['category_id'=> $id]);

		foreach ($data as $key => $value) {
			$value->items = ProductFiltersItems::where('product_filters_id',$value->id)->get();
		}

		return $data;
	}
 
}