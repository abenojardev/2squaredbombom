<?php 

namespace App\Repositories;
  
use Illuminate\Database\Eloquent\Model; 
use App\Contracts\FilterItemsInterface; 
use App\Repositories\ResourceRepo;

class FilterItemsRepository extends ResourceRepo implements FilterItemsInterface
{
	
	protected $model; 

	function __construct(Model $model)
	{
		$this->model = $model; 
	}  

	public function getFiltersItemsForTables($id)
	{
		return $this->getAll(['product_filters_id'=> $id]); 
	}

}