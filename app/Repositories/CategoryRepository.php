<?php 

namespace App\Repositories;
  
use Illuminate\Database\Eloquent\Model; 
use App\Contracts\CategoryInterface; 
use App\Repositories\ResourceRepo;

class CategoryRepository extends ResourceRepo implements CategoryInterface
{
	
	protected $model; 

	function __construct(Model $model)
	{
		$this->model = $model; 
	} 

	public function getCategoriesForTables($id)
	{
		$data = $this->getAll(['parent_id'=> $id]);

		foreach ($data as $key => $value) {
			$value->sub = $this->getCount(['parent_id'=>$value->id]);
		}

		return $data;
	}

	public function getParentCategory($id)
	{

		return is_null($id) ? null : $this->getById($id);
	}
 
	public function getParentBySlug($slug)
	{
		$slug = strtolower(str_replace('-', ' ', $slug));

		$data = $this->getAll(null,[
			[
				'col' => 'title',
				'operand' => 'LIKE',
				'val' => $slug
			]
		]);
 
		if ($data->count() <= 0) {
			abort(404);
		}

		return $this->getParentCategory($data->first()->id);
	}

	public function getCategoryBySlug($slug)
	{
		$slug = strtolower(str_replace('-', ' ', $slug));

		$data = $this->getAll(null,[
			[
				'col' => 'title',
				'operand' => 'LIKE',
				'val' => $slug
			]
		]);

		$sub = ($data->count() != 0) ? $this->getCategoriesForTables($data->first()->id) : [];

		return $sub;
	}
}