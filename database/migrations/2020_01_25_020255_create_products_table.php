<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->string('type'); // main / inherited
            $table->integer('product_id')->nullable(); 
            $table->integer('category_id')->nullable(); 
            $table->string('name')->nullable();
            $table->longText('description')->nullable();
            $table->longText('part_number')->nullable();
            $table->integer('stocks')->nullable();
            $table->string('srp')->nullable();
            $table->longText('photo')->nullable();
            $table->longText('filters')->nullable();
            $table->longText('specs')->nullable();
            $table->integer('is_available')->nullable();
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
