 $(function() {
    // Basic example
    $('#dt-base').DataTable();
    // Responsive
    $('#dt-responsive').DataTable({
        responsive: true,
        ajax: './assets/demo/data/datatable-data.json',
    });
    // Scroll - vertical example
    $('#dt-scroll-vertical').DataTable({
        scrollY: "250px",
        scrollCollapse: true,
        paging: false,
        ajax: './assets/demo/data/datatable-data.json',
    });
    // Scroll - horizontal
    $('#dt-scroll-horizonal').DataTable({
        scrollX: true
    });
    // Ajax sourced data
    $('#dt-ajax-data').DataTable({
        ajax: './assets/demo/data/datatable-data.json',
        responsive: true,
    });
    // Buttons example
    $('#dt-buttons').DataTable({
        ajax: './assets/demo/data/datatable-data.json',
        responsive: true,
        dom: 'Bfrtip',
        buttons: [
            'copy', 'excel', 'pdf', 'csv', 'print'
        ]
    });
    // Filter & custom search field
    $(function() {
        var table = $('#dt-filter').DataTable({
            responsive: true,
            "sDom": 'rtip',
            columnDefs: [{
                targets: 'no-sort',
                orderable: false
            }]
        });
        $('#key-search').on('keyup', function() {
            table.search(this.value).draw();
        });
        $('#type-filter').on('change', function() {
            table.column(4).search($(this).val()).draw();
        });
    });
}); 