<?php 

/*
|--------------------------------------------------------------------------
| Resources Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['namespace' => 'Frontend'],function(){

	Route::group(['namespace' => 'Pages'],function(){
 
		Route::get('/',                     'HomepageController@index'                      )->name('client.homepage'); 
		Route::get('/category/{cat}',       'HomepageController@categoriesview'             )->name('client.categories');
		Route::get('/products/{name}',      'HomepageController@productview'                )->name('client.products'); 
		Route::get('/cart',                 'HomepageController@cart'                       )->name('client.cart');   
		Route::get('/checkout',             'HomepageController@checkout'                   )->name('client.cart.checkout');  
		Route::get('/cart/clear',           'HomepageController@clearcart'                  )->name('client.cart.clear'); 
		Route::post('/addtocart',           'HomepageController@addtocart'                  )->name('client.products.addtocart'); 
		Route::get('/compare/',             'HomepageController@compare'                    )->name('client.compare'); 
		Route::get('/compare/{id}',         'HomepageController@addtocompare'               )->name('client.compare.add'); 
		Route::get('/clear/compare',        'HomepageController@removeItemsFromCompare'     )->name('client.compare.clear');  

		Route::get('/login',                'HomepageController@login'                      )->name('client.login');  
		Route::post('/register',                'HomepageController@register'                      )->name('client.register');  
		Route::post('/verify',                'HomepageController@verify'                      )->name('client.verify');  
		Route::get('/logout',                'HomepageController@logout'                      )->name('client.logout');   
		Route::post('/checkout/save',                'HomepageController@savecheckout'                      )->name('client.checkout.save');  
		Route::get('/quotations',                'HomepageController@quotations'                      )->name('client.quotations');   
		Route::get('/quotations/view/{id}',                'HomepageController@quotationsview'                      )->name('client.quotation.view');   

	});
});