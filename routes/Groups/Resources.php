<?php 

/*
|--------------------------------------------------------------------------
| Resources Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => 'photos','namespace' => 'Resources'],function(){
	Route::get('/',                 'ImageController@index'   )->name('app.resources');
});