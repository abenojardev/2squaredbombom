<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use \App\Models\User;

class ActionTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    protected $admin;

    public function setUp() : void
    {
        parent::setUp(); 
        $this->admin = User::find(1); 
    }

    public function testLogin()
    {
        $response = $this->get('/backend');
        $response->assertStatus(200);
    }

    public function testLoginAction()
    { 
        $response = $this->actingAs($this->admin)->post('/backend/verify');

        $response->assertRedirect('/backend/dashboard');
    }

    public function testProductSearchView()
    {
        $response = $this->actingAs($this->admin)
                         ->get('backend/products/search');

        $response->assertStatus(200); 
    }

    public function testProductSearchSearch()
    {
        $response = $this->actingAs($this->admin)
                         ->get('backend/products/search?q=a');

        $response->assertStatus(200)
                 ->assertViewHasAll(['data','q','category']); 
    }

    public function testDashboard()
    {
        $response = $this->get('/backend/dashboard');
        $response->assertStatus(302);
    }
}
